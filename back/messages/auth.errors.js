// numbering: 41 - 80

module.exports = {
    blacklisted: {
        webCode: 403,
        refErr: 41,
    },
    InvalidToken:{
        webCode: 498,
        refErr: 42,
    },
    missingToken: {
        webCode: 401,
        refErr: 43,
    },
    tokenExtension: {
        webCode: 500,
        refErr: 44,
    },
    attemptViolationOtherUser: {
        webCode: 403,
        refErr: 45,
    }
}