import Vue from 'vue';
import VueI18n from 'vue-i18n';

// Language translation
Vue.use(VueI18n);

const messages = {
  fr: {
    login: require('./login.fr'),
    loginVue: require('./loginVue.fr'),
    accountVue: require('./accountVue.fr'),
    gameVue: require('./gameVue.fr'),
    footerVue: require('./footerVue.fr'),
    theme: require('./theme.fr'),
    menu: require('./menu.fr'),
    gameRoom: require('./gameRoom.fr'),
  },
  en: {
    login: require('./login.en'),
    loginVue: require('./loginVue.en'),
    accountVue: require('./accountVue.en'),
    gameVue: require('./gameVue.en'),
    footerVue: require('./footerVue.en'),
    theme: require('./theme.en'),
    menu: require('./menu.en'),
    gameRoom: require('./gameRoom.en'),
  }
}

const i18n = new VueI18n({ 
  locale: 'fr', 
  fallbackLocale: 'en',  // if text does not exist in the locale dictionary, try the fallbackLocale
  messages: messages
});

// Hot updates
if (module.hot) {
  module.hot.accept(['./loginVue.en', './loginVue.fr'], function () {
    i18n.setLocaleMessage('en', require('./loginVue.en').default)
    i18n.setLocaleMessage('fr', require('./loginVue.fr').default)
  })
}

export default i18n;