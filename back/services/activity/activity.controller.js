// controller created for the future,
// currently unused

const express = require('express');
const activityService = require('./activity.service');

const router = express.Router();

// Set routes
router.delete('/', removeAll);
router.post('/record/:ip', record);
router.post('/signal/:ip', signal);
module.exports = router;

// Set actions
/**
 * Adds or updates statistics, can trigger the banning of an IP address if it exceeds a certain number of requests per minute.
 * @route POST /record/:ip
 * @group activity - Operations about activity (router not connected)
 * @param {String} ip.query.required - ip associated with statistics
 */
function record(req, res, next) {
  activityService.record(req.params.ip)
    .then(() => res.json({}))
    .catch(err => next(err));
}

/**
 * Used to report an attempted fraudulent action, can trigger the banning of an IP address.
 * @route POST /signal/:ip
 * @group activity - Operations about activity (router not connected)
 * @param {String} ip.query.required - ip associated with statistics
 */
function signal(req, res, next) {
  activityService.signal(req.params.ip)
    .then(() => res.json({}))
    .catch(err => next(err));
}

/**
 * Remove all activities (for test only)
 * @route DELETE /
 * @group activity - Operations about activity (router not connected)
 */
function removeAll(req, res, next) {
  activityService.removeAll()
    .then(() => res.json({}))
    .catch(err => next(err));
}