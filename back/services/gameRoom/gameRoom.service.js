﻿﻿//const config = require('../../config');
const gameRoomErrorMsg = require('../../messages/gameRoom.errors.js');
const GameRoom = require('./gameRoom.model');
const User = require('../user/user.model');
const Music = require('../music/music.model');
//const WebSocket = require('../../websockets/websocket');

async function ownerElection(roomId, params=null) {
  if(!params) {
    params = await GameRoom.findById(roomId).select('players');
  }

  // select a new owner
  const playerIds = Object.keys(params.players);
  const newOwnerId = playerIds[Math.floor(Math.random() * playerIds.length)];
  params.owner = newOwnerId;
  params.ownerSocket = params.players[newOwnerId].socket;
  params.ownerStatus = 'ready';
  
  return params;
}

async function remove(roomId) {
  const execState = await GameRoom.deleteOne({ _id: roomId });
  console.log("gameRoom remove "+roomId+": "+execState.deletedCount);
}

/**************  Websockets  **************/
/******************************************/
async function booking(ownerSocket, ownerId, settings) {
  if(!ownerId || !settings) throw gameRoomErrorMsg.invalidInput;

  var ownerDara = await User.findById(ownerId).select('username -_id');
  var params = {
    owner: ownerId,
    ownerSocket: ownerSocket.id,
    ownerName: ownerDara.username,
    players: {},
    gameMode: settings.gameMode,
    difficulty: settings.difficulty,
    multiplayer: settings.multiplayer
  }
  /*params.players[ownerId] = {
    socket: requesterSocket.id, 
    connection: 'ready'
  };*/
  //newParams.markModified('players');
  if(settings.multiplayer) {
    params.playerLimit = settings.playerLimit;
    params.publicRoom = settings.publicRoom;
    params.multiplayerMode = settings.multiplayerMode;
  } else {
    params.playerLimit = -1;
    params.publicRoom = true;
    params.multiplayerMode = 'null';
  }
  console.log('params:', params);
  
  const gameRoom = new GameRoom(params);
  const execState = await gameRoom.save();
  const roomId = execState._id;
  console.log("gameRoom - booking:",execState._id,":",execState.createdDate);

  // enroll the owner in the websocket room corresponding to the game room
  await ownerSocket.join(roomId);

  // connect the user
  return roomId;
}

async function playerJoin(requesterSocket, requesterId, roomId) {
  //console.log('playerJoin - roomId:',roomId,'; requesterId:',requesterId);
  
  var newParams = await GameRoom.findById(roomId).select('players blacklist owner ownerSocket -_id');
  if (!newParams) throw gameRoomErrorMsg.gameRoomNotFound;
  // check blacklist
  if (newParams.blacklist.includes(requesterId)) throw gameRoomErrorMsg.blacklisted;

  console.log('playerJoin - requesterId:', requesterId);
  console.log('newParams.owner:',newParams.owner);

  if (requesterId != newParams.owner && !newParams.players[requesterId]) {
    // get player info
    var playerData = await User.findById(requesterId).select('username -_id');
    var newPlayer = {
      socket: requesterSocket.id, 
      username: playerData.username,
      rank: 0,
      score: 0,
      avatar: '',
      connection: 'connexion...'
    };

    newParams.players[requesterId] = newPlayer;
    //newParams.markModified('players');
    console.log('playerJoin - newParams:', newParams);
    console.log('playerJoin - newParams.players:', newParams.players);

    // save player attachment
    const execState = await GameRoom.updateOne({ _id: roomId }, newParams);
    console.log("gameRoom - playerJoin", roomId, ":", execState.nModified, ", found "+execState.n);

    // enroll the player in the websocket room corresponding to the game room
    requesterSocket.join(roomId);

    // broadcast information of a new player in the game room
    
    await requesterSocket.to(roomId).emit('playerChange', {
      action: 'add',
      // TODO: get information
      playerId: requesterId,
      player: newPlayer
    })
  }
}

async function synchronization(requesterSocket, requesterId, roomId) {
  var newParams = await GameRoom.findById(roomId).select('players owner ownerSocket ownerName');
  if (!newParams) throw gameRoomErrorMsg.gameRoomNotFound;
  if (!newParams.players[requesterId]) throw gameRoomErrorMsg.playerNotFound;

  // ask the game room owner about room information
  console.log('synchronization - newParams.ownerSocket:', newParams.ownerSocket);
  
  await requesterSocket.to(newParams.ownerSocket).emit('synchronizationMusic', {
    requesterSocket: requesterSocket.id,
    requesterId: requesterId
  })

  await requesterSocket.emit('synchronizationPlayersR', {
    owner: newParams.owner,
    ownerName: newParams.ownerName,
    params: newParams.players
  })
  /*await requesterSocket.to(newParams.ownerSocket).emit('synchronizationPlayers', {
    requesterSocket: requesterSocket.id,
    requesterId: requesterId
  })*/
}

async function synchronizationMusicR(requesterSocket, requesterId, roomId, targetSocket, targetId, params) {
  var newParams = await GameRoom.findById(roomId).select('players owner');
  if (!newParams) throw gameRoomErrorMsg.gameRoomNotFound;
  if (requesterId != newParams.owner) throw gameRoomErrorMsg.notTheOwner;
  if (!newParams.players[targetId]) throw gameRoomErrorMsg.playerNotFound;

  // forward data
  await requesterSocket.to(targetSocket).emit('synchronizationMusicR', { params: params });
}

/*async function synchronizationPlayersR(requesterSocket, requesterId, roomId, targetSocket, targetId, params) {
  var newParams = await GameRoom.findById(roomId).select('players owner');
  if (!newParams) throw gameRoomErrorMsg.gameRoomNotFound;
  if (requesterId != newParams.owner) throw gameRoomErrorMsg.notTheOwner;
  if (!newParams.players[targetId]) throw gameRoomErrorMsg.playerNotFound;

  // forward data
  await requesterSocket.to(targetSocket).emit('synchronizationPlayersR', { params: params });
}*/

async function synchronized(requesterSocket, requesterId, roomId) {
  var newParams = await GameRoom.findById(roomId).select('players');
  if (!newParams) throw gameRoomErrorMsg.gameRoomNotFound;
  if (!newParams.players[requesterId]) throw gameRoomErrorMsg.playerNotFound;
  // save new player status
  newParams.players[requesterId].connection = 'ready';   //TODO: check if works
  newParams.markModified('players');
  const execState = await GameRoom.updateOne({ _id: roomId }, newParams);
  console.log("gameRoom - isReady", roomId, ":", execState.nModified, ", found "+execState.n);
  // broadcast information of a player synchronization status change
  await requesterSocket.in(roomId).emit('playerChange', {
    action: 'update',
    playerId: requesterId,
    player: {
      connection: 'ready'
    }
  })
}

async function playerLeaveFromSocket(requesterSocket, roomId) {
  var requesterId = null;
  var params = await GameRoom.findById(roomId).select('players owner ownerSocket -_id');
  if (params.ownerSocket == requesterSocket.id) {
    requesterId = params.owner;
  } else {
    Object.keys(params.players).find((playerId) => {
      if (params.players[playerId].socket == requesterSocket.id) {
        requesterId = playerId;
        return true;
      }
    })
  }

  console.log(`player ${(requesterId) ? requesterId : '???'} leaves the room ${roomId}`);
  if (requesterId) playerLeave(requesterSocket, requesterId, roomId);
}

async function playerLeave(requesterSocket, requesterId, roomId) {
  var newParams = await GameRoom.findById(roomId).select('players playlist owner -_id');

  if (!newParams) throw gameRoomErrorMsg.gameRoomNotFound;
  if (requesterId == newParams.owner || newParams.players[requesterId]) {
    var change = {
      action: 'delete',
      playerId: requesterId
    };
    if (requesterId == newParams.owner) {
      // owner case
      if (Object.keys(newParams.players).length == 0) {
        // remove the game room
        await remove(roomId);
        return;
      } else {
        console.log('newParams.playlist.length:', newParams.playlist.length);

        newParams = await ownerElection(roomId, newParams);
        var ownerDara = await User.findById(newParams.owner).select('username -_id');
        change.newOwner = {
          id: newParams.owner,
          name: ownerDara.username,
          socket: newParams.ownerSocket,
          removeLocalFiles: (newParams.playlist.length == 0)
        };
      }
    } else {
      delete newParams.players[requesterId];
      if(!Object.keys(newParams.players).length) {
        newParams.players = undefined;
      }
    }

    // save player deletion
    if (newParams.players) {
      const execState = await GameRoom.updateOne({ _id: roomId }, newParams);
      console.log("gameRoom - playerLeave", roomId, ":", execState.nModified, ", found "+execState.n);
    } else {
      console.log("remove player list ...");
      const execState = await GameRoom.updateOne({ _id: roomId }, { 
        owner: newParams.owner,
        ownerSocket: newParams.ownerSocket,
        $unset: { players: "" }
      });
      console.log("gameRoom - playerLeave", roomId, ":", execState.nModified, ", found "+execState.n);
    }
    
    // broadcast information of a player synchronization change
    await requesterSocket.to(roomId).emit('playerChange', change);
  }
}

async function kickPlayer(requesterSocket, requesterId, roomId, targetId) {
  var newParams = await GameRoom.findById(roomId).select('players owner');
  if (!newParams) throw gameRoomErrorMsg.gameRoomNotFound;
  if (requesterId != newParams.owner) throw gameRoomErrorMsg.notTheOwner;

  playerLeave(requesterSocket, targetId, roomId);
}

async function banPlayer(requesterSocket, requesterId, roomId, targetId) {
  var newParams = await GameRoom.findById(roomId).select('blacklist owner');
  if (!newParams) throw gameRoomErrorMsg.gameRoomNotFound;
  if (requesterId != newParams.owner) throw gameRoomErrorMsg.notTheOwner;

  if(newParams.blacklist.indexOf(targetId) == -1) {
    newParams.blacklist.push(targetId);
    // save blacklisting
    const execState = await GameRoom.updateOne({ _id: roomId }, newParams);
    console.log("gameRoom - banPlayer", roomId, ":", execState.nModified, ", found "+execState.n);

    // force player to leave
    playerLeave(requesterSocket, targetId, roomId);
  }
}

async function loseOwner(requesterSocket, requesterId, roomId) {
  var newParams = await GameRoom.findById(roomId).select('players ownerSocket ownerStatus');
  if (!newParams) throw gameRoomErrorMsg.gameRoomNotFound;
  if (newParams.players[requesterId]) throw gameRoomErrorMsg.playerNotFound;
  
  // change owner status
  newParams.ownerStatus = 'inValidation';
  const execState = await GameRoom.updateOne({ _id: roomId }, newParams);
  console.log("gameRoom - loseOwner", roomId, ":", execState.nModified, ", found "+execState.n);

  // checkPresence owner
  await requesterSocket.to(ownerSocket).emit('checkPresence', {});
  setTimeout(async function () {
    const checkStatus = await GameRoom.findById(roomId).select('ownerStatus');
    if (checkStatus) {
      if (checkStatus.ownerStatus == 'inValidation') {
        var newParams = ownerElection(roomId);
        // save owner change
        const execState = await GameRoom.updateOne({ _id: roomId }, newParams);
        console.log("gameRoom - loseOwner", roomId, ":", execState.nModified, ", found "+execState.n);

        // notify the room
        await requesterSocket.in(roomId).emit('ownerChange', { id: newParams.owner });
      }
    }
  }, 800); 
}

async function pong(requesterId, roomId) {
  var newParams = await GameRoom.findById(roomId).select('owner');
  if (!newParams) throw gameRoomErrorMsg.gameRoomNotFound;
  if (requesterId != newParams.owner) throw gameRoomErrorMsg.notTheOwner;
  // valide owner status
  newParams.ownerStatus = 'ready';
  const execState = await GameRoom.updateOne({ _id: roomId }, newParams);
  console.log("gameRoom - pong", roomId, ":", execState.nModified, ", found "+execState.n);
}

async function playlistChange(requesterSocket, requesterId, roomId, action, songData) {
  var newParams = await GameRoom.findById(roomId).select('owner');
  if (!newParams) throw gameRoomErrorMsg.gameRoomNotFound;
  if (requesterId != newParams.owner) throw gameRoomErrorMsg.notTheOwner;
  if (
    !songData || 
    !action || 
    (action && !action.match('^(add|delete)$'))
  ) throw gameRoomErrorMsg.invalidInput;

  // broadcast the change
  requesterSocket.to(roomId).emit('playlistChange', {
    action: action,
    song: songData
  });
}

async function lockPlaylist(requesterSocket, requesterId, roomId, playlist) {
  const gameRoom = await GameRoom.findById(roomId).select('owner playlist playlistLocked');
  if (!gameRoom) throw gameRoomErrorMsg.gameRoomNotFound;
  if (requesterId != gameRoom.owner) throw gameRoomErrorMsg.notTheOwner;
  if (!gameRoom.playlistLocked) {
    var newParams = {
      playlist: playlist,
      playlistLocked: true
    }
    // save game room
    const execState = await GameRoom.updateOne({ _id: roomId }, newParams);
    console.log("gameRoom updated "+roomId+": "+execState.nModified+", found "+execState.n);

    // send a confirmation
    requesterSocket.emit('lockPlaylistR', {});
  } else {
    throw gameRoomErrorMsg.playlistAlreadyLocked;
  }
}

async function blockBooking(requesterSocket, requesterId, roomId) {
  console.log('blockBooking');
  
  var newParams = await GameRoom.findById(roomId).select('owner');
  if (!newParams) throw gameRoomErrorMsg.gameRoomNotFound;
  if (requesterId != newParams.owner) throw gameRoomErrorMsg.notTheOwner;

  // save game room
  newParams = {
    status: 'blocked'
  }
  const execState = await GameRoom.updateOne({ _id: roomId }, newParams);
  console.log("gameRoom updated "+roomId+": "+execState.nModified+", found "+execState.n);

  // broadcast the beginning of the party
  await requesterSocket.in(roomId).emit('gameBeginning', {});
}


/****************  routes  ****************/
/******************************************/
async function roomList(multiplayerMode, gameMode, difficulty) {
  const filter = {
    publicRoom: true
  };
  if (multiplayerMode) filter.push(multiplayerMode);
  if (gameMode) filter.push(gameMode);
  if (difficulty) filter.push(difficulty);
  const gameRoom = await GameRoom.find(filter);
  //console.log('-----------------------------------------------');
  //console.log('gameRoom:',gameRoom);
  
  return gameRoom;
}

/*async function roomInfo(id, userId) {
  const gameRoom = await GameRoom.findById(id).select('-__v -createdDate');
  if (!gameRoom) throw gameRoomErrorMsg.gameRoomNotFound;
  if (
    userId != gameRoom.owner &&
    !gameRoom.players.includes(userId)
  ) throw gameRoomErrorMsg.unauthorized;
  return gameRoom;
}*/

async function removeAll() {
  const execState = await GameRoom.deleteMany({});  
  console.log("gameRoom removeAll : "+execState.deletedCount+", found "+execState.n);
}

async function nextTrack(requesterId, roomId) {
  console.log('nextTrack - requesterId:',requesterId, '; roomId:', roomId );
  
  const gameRoom = await GameRoom.findById(roomId).select('owner playlist');
  if (!gameRoom) throw gameRoomErrorMsg.gameRoomNotFound;
  if (requesterId != gameRoom.owner) throw gameRoomErrorMsg.notTheOwner;

  // get first song and remove it from the list
  const track = gameRoom.playlist.shift();
  var music = await Music.findById(track).select('-_id');

  // save game room
  const execState = await GameRoom.updateOne({ _id: roomId }, gameRoom);
  console.log("gameRoom updated "+roomId+": "+execState.nModified+", found "+execState.n);

  // return the song
  return { track: music };
}


module.exports = {
  // websockets
  booking, 
  playerJoin, 
  synchronization, 
  synchronizationMusicR, 
  //synchronizationPlayersR, 
  synchronized, 
  playerLeaveFromSocket,
  playerLeave, 
  kickPlayer, 
  banPlayer, 
  loseOwner, 
  pong, 
  playlistChange, 
  lockPlaylist,
  blockBooking,
  // routes
  roomList,
  //roomInfo,
  remove,
  removeAll,
  nextTrack
};
