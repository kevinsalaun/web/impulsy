/**
 * @typedef Activity
 * @property {string} ip.required - ip
 * @property {Number} req - request counter, 1 by default
 * @property {Number} reqLastMinute - counter of requests during the last minute, 1 by default
 * @property {Date} lastMinute - lastMinute, Date.now by default
 * @property {Number} unauthorizedActions - unauthorizedActions, 0 by default
 * @property {Date} createdDate - createdDate, Date.now by default
 * @property {Date} expireAt - expireAt, Date.now by default (the record is deleted upon expiry - 1d)
 */

const Storage = require('../storage');
const storage = new Storage().getInstance();

model = storage.model('Activity', {
  ip: { type: String, unique: true, required: true },
  req: { type: Number, default: 1 },
  reqLastMinute: { type: Number, default: 1 },
  lastMinute: { type: Date, default: Date.now },
  unauthorizedActions: { type: Number, default: 0 },
  createdDate: { type: Date, default: Date.now },
  expireAt: { type: Date, default: Date.now, index: { expires: '1d' } }
});
  
module.exports = model;
