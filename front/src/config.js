config = {
  app: {
    defaultLanguage: 'fr'
  },
  backApi: {
    uri: 'http://backend-impulsy.local:8085/',
    routes: {
      create: 'user/',
      authenticate: 'user/',
      update: 'user/<id>',
      delete: 'user/<id>',
      getCurrent: 'user/<id>'
    }
  }
};

module.exports = config;