/**
 * @typedef GameRoom
 * @property {string} owner.required - owner of the room
 * @property {string} players.required - players in the room
 * @property {string} playlist.required - playlist
 * @property {Date} settings.required - settings (game mode, public room...)
 * @property {string} status - status, 'booked' by default
 * @property {Date} createdDate - createdDate, Date.now by default
 */

const Storage = require('../storage');
const storage = new Storage().getInstance();
const ObjectId = storage.mongoose.Types.ObjectId;

/*var vfArray = [
  {
    field: 'slots',
    fct: function () {
      return this.players.length + '/' + this.name.playerLimit;
    }
  }
]*/

model = storage.model('GameRoom', {
  owner: { type: ObjectId, ref: 'Users', required: true },
  ownerName: { type: String, required: true },
  ownerSocket: { type: String, required: true },
  ownerStatus: { type: String, enum: ['ready', 'inValidation'], default: 'ready' },
  players: { type: Object, default: {} },
  blacklist: { type: Array, default: [] },
  playlist: { type: Array, default: [] },
  playlistLocked: { type: Boolean, default: false },
  publicRoom: { type: Boolean, default: false },
  multiplayer: { type: Boolean, required: true },
  playerLimit: { type: Number, default: -1 },
  multiplayerMode: { type: String, enum: ['coop', 'compet', 'null'], default: 'null'  },
  gameMode: { type: String, enum: ['classic', 'broken', 'drummer'], required: true  },
  difficulty: { type: String, enum: ['lazy', 'easy', 'crazy'], required: true  },
  status: { type: String, enum: ['booked', 'blocked'], default: 'booked' },
  createdDate: { type: Date, default: Date.now },
  expireAt: { type: Date, default: Date.now, index: { expires: '1d' } }
}); //, vfArray);
  
module.exports = model;
