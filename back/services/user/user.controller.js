const express = require('express');
const restricted = require('../../middleware/restricted');
const userService = require('./user.service');

const router = express.Router();

// Set routes
router.delete('/', removeAll);
router.put('/', create);
router.post('/', authenticate);
router.post('/:id', restricted, update);
router.delete('/:id', restricted, remove);
router.get('/:id', restricted, getCurrent);
module.exports = router;

// Set actions
/**
 * Registers a user
 * @route PUT /
 * @group user - Operations about user
 * @param {String} username.body.required - user's username
 * @param {String} email.body.required - user's email
 * @param {String} password.body.required - user's password
 * @param {Date} birthday.body.required - user's birthday
 * @param {String} role.body.required - user's role
 * @returns {object} an access token and an expiry date in millisecond format.
 */
function create(req, res, next) {
  console.log("user.controller->create");
  userService.create(req.body)
    .then(token => res.json(token))
    .catch(err => next(err));
}

/**
 * Authenticates the user
 * @route POST /
 * @group user - Operations about user
 * @param {String} username.body.required - user's username (useless if email address provided)
 * @param {String} email.body.required - user's email (useless if username provided)
 * @param {String} password.body.required - user's password
 * @returns {object} an access token and an expiry date in millisecond format.
 */
function authenticate(req, res, next) {
  console.log("user.controller->authenticate");
  userService.authenticate(req.body)
    .then(token => res.json(token))
    .catch(err => next(err));
}

/**
 * Updates user information
 * @route POST /:id
 * @group user - Operations about user
 * @param {String} id.query.required - user's id
 * @param {String} username.body - user's username
 * @param {String} email.body - user's email
 * @param {String} password.body - user's password
 * @param {Date} birthday.body - user's birthday
 * @param {String} role.body - user's role
 * @HeaderParam {String} Authorization.body - user's access token (format: Bearer <token>)
 * @security JWT
 */
function update(req, res, next) {
  console.log("user.controller->update");
  userService.update(req.params.id, req.body)
    .then((user) => res.json({
      ...req.toReturn,
      ...user
    }))
    .catch(err => next(err));
}

/**
 * Remove the user
 * @route DELETE /:id
 * @group user - Operations about user
 * @param {String} id.query.required - user's id
 * @HeaderParam {String} Authorization.body - user's access token (format: Bearer <token>)
 * @security JWT
 */
function remove(req, res, next) {
  console.log("user.controller->remove");
  userService.remove(req.params.id)
    .then(() => res.json({}))
    .catch(err => next(err));
}

/**
 * Gets user information
 * @route GET /:id
 * @group user - Operations about user
 * @param {String} id.query.required - user's id
 * @HeaderParam {String} Authorization.body - user's access token (format: Bearer <token>)
 * @security JWT
 * @returns {object} current user
 */
function getCurrent(req, res, next) {
  userService.getCurrent(req.params.id)
    .then(user => res.json({
      ...req.toReturn,
      ...user
    }))
    .catch(err => next(err));
}

/**
 * Remove all users (for test only)
 * @route DELETE /
 * @group user - Operations about user
 */
function removeAll(req, res, next) {
  userService.removeAll()
    .then(() => res.json({}))
    .catch(err => next(err));
}