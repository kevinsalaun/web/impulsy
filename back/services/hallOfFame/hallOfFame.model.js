const Storage = require('../storage');
const Musics = require("../music/music.model")
const Users = require("../user/user.model")

const storage = new Storage().getInstance();
const ObjectId = storage.mongoose.Types.ObjectId;

model = storage.model('HallOfFame', {
	//id: { type: String, required: true },
	score: { type: Number, required: true },
	scoreDate: { type: Date, default: Date.now },
	difficulty: { type: String, enum: ['lazy', 'easy', 'crazy'], required: true },
	gameMode: { type: String, enum: ['classic', 'broken', 'drummer'], required: true },
	multiplayerMode: { type: String, enum: ['coop', 'compet'], required: true },
	idUser: { type: ObjectId, ref: 'User', required: true },
	idMusic: { type: ObjectId, ref: 'Music' }  //, required: true} à remettre plus tard
});
  
module.exports = model;
