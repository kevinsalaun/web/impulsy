const fs = require('fs');
//const config = require('../../config');
const Musics = require('./music.model');
const errorMsg = require('../../messages/music.errors');
const ext = require('./music.service.ext');


async function getAllMusics () {
  var musics = Musics.find();
  return musics;
}

async function getSpecificMusic (id) {
  var music = Musics.findById(id)
  return music;
}

async function create(fields, files, musicLocation) {
  try {
    // Create the tmp folder
    if (!fs.existsSync(musicLocation)) {
      fs.mkdirSync(musicLocation);
    }

    var musicIdList = [];
    for (var id in files) {
      // Get the filename (local files are automatically downloaded)
      const filePath = files[id][0].path;
      if (fields[id]) {
        const description = JSON.parse(fields[id]);
        
        // Treat and save the music data
        const music = new Musics(await ext.treatAndSaveMusic(filePath, description));
        const res = await music.save();
        musicIdList.push(res._id);
      }

      delete fields[id];
    }
    for (var id in fields) {
      // Generate a filename
      const description = JSON.parse(fields[id]);
      const fileID = await ext.getFileID(musicLocation);
      const filePath = musicLocation + fileID;

      //console.log('description:', description);
      

      // Download songs
      if (description.platform == 'youtube') {
        await ext.downloadYtb(filePath, description.ytTag);  // (tag youtube: url_youtube?v=######)
      }

      //console.log('filePath:',filePath)
      if (fs.existsSync(filePath)) {
        // Treat and save the music data
        const music = new Musics(await ext.treatAndSaveMusic(filePath, description));
        const res = await music.save();
        musicIdList.push(res._id);
      } else {
        throw 'platform not supported';
      }
    }
    return musicIdList;
  } catch (error) {
    // clean music directory
    ext.cleanMusicDir(musicLocation);
    throw error
  }

  // clean music directory
  //ext.cleanMusicDir(musicLocation);

  // print entry
  //console.log(getAllMusics());
}

async function remove(id) {
  await Musics.findByIdAndRemove(id);
}

async function removeAll() {
  const execState = await Musics.deleteMany({});  
  console.log("music removeAll : "+execState.deletedCount+", found "+execState.n);
}

async function update(id, musicParam) {
  const oldMusic = await Musics.findById(id);

  // validate
  if (!oldMusic) throw errorMsg.musicNotFound;

  const music = new Musics(musicParam);

  // save user
  await music.save();
}


module.exports = {
  getAllMusics,
  getSpecificMusic,
  create,
  remove,
  removeAll
};
