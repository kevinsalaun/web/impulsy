var axios = require('axios');
var config = require('../config');

/*function getErrorMsgByID(errorList, id) {
    var res = null;
    for(let idx in errorList) {
        const e = errorList[idx];
        if(e.id == id) {
            res = e.msg;
            break;
        }
    }
    return res;
}*/

function getErrorMsgByIdx(errorList, errorId) {
    console.log('errorId:', errorId);
    for (var i = 0; i < errorList.length; i++) {
        if (errorList[i][errorId]) {
            return errorList[i][errorId].msg;
        }
    }
    return 'unknown error';
}

/*function booking(errorList, token, settings) {
    return new Promise(function(resolve, reject) {
        console.log('booking of a game room...')
        
        axios
            .put(config.backApi.uri + config.backApi.routes.gameRoom.booking,
                {
                    settings: settings
                },
                {
                    headers: { 
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    timeout: 1000
            })
            .then(resp => {
                console.log('game room booked (',resp.data.id,')')
                resolve(resp.data.id);
            })
            .catch(err => {
                if(err.response) reject(getErrorMsgByIdx(errorList, err.response.data.error));
                else reject();
            });
    });
}*/

/*function blockBooking(errorArray, token, roomId, playlist) {
    return new Promise(function(resolve, reject) {
        console.log('block the booking of the game room...')
        axios
            .post(config.backApi.uri + config.backApi.routes.gameRoom.blockBooking.replace('<id>', roomId),
                {
                    playlist: playlist
                },
                {
                    headers: { 
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    timeout: 1000
                })
            .then(() => {
                console.log('booking blocked')
                resolve();
            })
            .catch(err => {
                if(err.response) reject(getErrorMsgByIdx(errorArray, err.response.data.error));
                else reject();
            });
    });
}*/

/*function join(errorList, token, roomId) {
    return new Promise(function(resolve, reject) {
        console.log('join the game room', roomId, '...')
        axios
            .post(config.backApi.uri + config.backApi.routes.gameRoom.playerJoin.replace('<id>', roomId),
                {},
                {
                    headers: { 
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    timeout: 1000
                })
            .then(resp => {
                console.log('the user joined the game:',resp.data)
                resolve(resp.data);
            })
            .catch(err => {
                if(err.response) reject(getErrorMsgByIdx(errorList, err.response.data.error));
                else reject();
            });
    });
}

function leave(errorList, token, roomId) {
    return new Promise(function(resolve, reject) {
        console.log('leave the game room', roomId, '...')
        axios
            .post(config.backApi.uri + config.backApi.routes.gameRoom.playerLeave.replace('<id>', roomId),
                {},
                {
                    headers: { 
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    timeout: 1000
                })
            .then(() => {
                console.log('the user leaved the game')
                resolve();
            })
            .catch(err => {
                if(err.response) reject(getErrorMsgByIdx(errorList, err.response.data.error));
                else reject();
            });
    });
}

function kick(errorList, token, roomId, userId) {
    return new Promise(function(resolve, reject) {
        console.log('kick the user', userId, '...')
        axios
            .post(config.backApi.uri + config.backApi.routes.gameRoom.kickPlayer.replace('<id>', roomId),
                {
                    userId: userId
                },
                {
                    headers: { 
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    timeout: 1000
                })
            .then(() => {
                console.log('the user was kicked')
                resolve();
            })
            .catch(err => {
                if(err.response) reject(getErrorMsgByIdx(errorList, err.response.data.error));
                else reject();
            });
    });
}

function remove(errorList, token, roomId) {
    return new Promise(function(resolve, reject) {
        console.log('remove the game room', roomId, '...')
        axios
            .post(config.backApi.uri + config.backApi.routes.gameRoom.remove.replace('<id>', roomId),
                {},
                {
                    headers: { 
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    timeout: 1000
                })
            .then(() => {
                console.log('the user removed the game')
                resolve();
            })
            .catch(err => {
                if(err.response) reject(getErrorMsgByIdx(errorList, err.response.data.error));
                else reject();
            });
    });
}*/

function roomsList(errorArray, token, filter = {}) {
    return new Promise(function(resolve, reject) {
        axios
            .get(
                config.backApi.uri + config.backApi.routes.gameRoom.roomList,
                {
                    ...filter
                },
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    timeout: 1000
                }
            )
            .then(res => {
                resolve(res.data);
            })
            .catch(err => {
                if (err.response)
                    reject(
                        getErrorMsgByIdx(errorArray, err.response.data.error)
                    );
                else reject();
            });
    });
}

/*function roomInfo(errorList, token, roomId) {
    return new Promise(function(resolve, reject) {
        axios
            .get(config.backApi.uri + config.backApi.routes.gameRoom.roomInfo.replace('<id>', roomId),
                {},
                {
                    headers: { 
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    timeout: 1000
                })
            .then((res) => {
                resolve(res.data);
            })
            .catch(err => {
                if(err.response) reject(getErrorMsgByIdx(errorList, err.response.data.error));
                else reject();
            });
    });
}*/

/*function lockPlaylist(errorArray, token, roomId) {
    return new Promise(function(resolve, reject) {
        axios
            .post(config.backApi.uri + config.backApi.routes.gameRoom.lockPlaylist.replace('<id>', roomId),
                {},
                {
                    headers: { 
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    timeout: 1000
                })
            .then(() => {
                resolve();
            })
            .catch(err => {
                if(err.response) reject(getErrorMsgByIdx(errorArray, err.response.data.error));
                else reject();
            });
    });
}*/

function nextTrack(errorArray, token, roomId) {
    return new Promise(function(resolve, reject) {
        axios
            .post(
                config.backApi.uri +
                    config.backApi.routes.gameRoom.nextTrack.replace(
                        '<id>',
                        roomId
                    ),
                {},
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    timeout: 1000
                }
            )
            .then(newTrack => {
                resolve(newTrack);
            })
            .catch(err => {
                if (err.response)
                    reject(getErrorMsgByIdx(errorArray, err.response.data.error);
                else reject();
            });
    });
}

module.exports = {
    //booking,
    /*blockBooking,
    join,
    leave,
    kick,
    remove,*/
    roomsList,
    /*roomInfo,
    lockPlaylist*/
    nextTrack
};
