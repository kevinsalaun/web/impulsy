const pages_path = __dirname+"/../views/pages/live/";
const models = require('../models');
const Twitter = require('twitter');
const config = require('../config/config')[global.env];
const credentials = require('../' + config.credentials);


function connect(){
/*https://www.facebook.com/v4.0/dialog/oauth?
  client_id={app-id}
  &redirect_uri={"https://www.domain.com/login"}
  &state={"{st=state123abc,ds=123456789}"} 
*/
}

module.exports = {
  connect
};


exports.displayLive = function(req, res){
	const idRaid = req.params.id;

	/*****************/
    /*    Twitter    */
    /*****************/

    models.raid.findByPk(idRaid).then(function(raid_found){
        if(raid_found === null) return res.redirect("/live");
        let text = "";
        if(raid_found.dataValues.hashtag !== null && raid_found.dataValues.hashtag !== ""){
            let client = new Twitter({
                consumer_key: credentials.twitter.TWITTER_CONSUMER_KEY,
                consumer_secret: credentials.twitter.TWITTER_CONSUMER_SECRET,
                access_token_key: credentials.twitter.TWITTER_ACCESS_TOKEN_KEY,
                access_token_secret: credentials.twitter.TWITTER_ACCESS_TOKEN_SECRET
            });
            const hashtag = (raid_found.dataValues.hashtag[0] === '#') ? raid_found.dataValues.hashtag : '#' + raid_found.dataValues.hashtag;
            client.get('search/tweets', {q: hashtag}, function(error, tweets, response) {
                let tweetsHtml = [];
                const getAllTweets = tweets.statuses.map(function(status){
                    return new Promise(function(resolve){
                        const id = status.id_str;
                        const name = status.screen_name;
                        const tweetDate = status.created_at;
                        client.get('statuses/oembed', {url: 'https://twitter.com/' + name + '/status/' + id})
                            .then(function(data){
                                tweetsHtml.push({date: new Date(tweetDate), html:data.html});
                                resolve();
                            });
                    });
                });
                Promise.all(getAllTweets).then(function(){
                    tweetsHtml.sort((a, b) => {
                        return a.date - b.date;
                    });
                    tweetsHtml.forEach((tweetHtml) => {
                        text += tweetHtml.html + '<br>';
                    });
                    renderAfterGettingParticipantDatas(text, res, idRaid);
                });
            });
        }else{
            renderAfterGettingParticipantDatas(text, res, idRaid);
        }
    });
};