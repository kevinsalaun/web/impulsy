const ms = require('ms');
const jwt = require('jsonwebtoken');
const config = require('../config');
const authErrorMsg = require('../messages/auth.errors');
const blacklistService = require('../services/blacklist/blacklist.service');

function processing(req, res, next) {
  req.toReturn = {}

  // Format of token
  // Authorization: Bearer <access_token>

  // Get auth header value
  const bearerHeader = req.headers['authorization'];
  
  // Check if bearer is defined
  if(typeof bearerHeader !== 'undefined') {
    // Split at the space and get the token
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];

    // Check the access_token.
    jwt.verify(bearerToken, config.db.tokenSecret, (err, authData) => {
      if(err || !authData) {
        next(authErrorMsg.InvalidToken);
        return;
      }

      const userFingerprint = {
        id:       authData.id,
        username: authData.username,
        email:    authData.email
      }

      // in the case of the request specifically concerns the user,
      // checks that the login corresponds to the token user
      if (req.params.id && req.params.id != userFingerprint.id) {
        next(authErrorMsg.attemptViolationOtherUser);
        return;
      }

      // Keep alive the user session when the user navigate on the site
      // Generate a refreshToken
      const refreshToken = jwt.sign(userFingerprint, config.db.tokenSecret, { expiresIn: config.policy.token.expire });
      if (!refreshToken) {
        next(authErrorMsg.tokenExtension);
        return;
      }
      //const { exp } = decode(refreshToken);
      req.toReturn.refreshToken = refreshToken;
      req.toReturn.expire = new Date(new Date().getTime() + ms(config.policy.token.expire))
      req.userFingerprint = userFingerprint;
    });
  }
  // Hand over
  next();
}

module.exports = (req, res, next) => {
  //console.log('auth->run');

  // checks if client is blacklisted
  const ip = req.connection.remoteAddress;
  if(ip) {
    blacklistService.isBanned(ip)
      .then(banned => {
        if(banned) {
          console.log('auth - ip blacklisted')
          next(authErrorMsg.blacklisted);
        } else {
          //console.log('auth - ip NOT blacklisted')
          processing(req, res, next);
        }
      })
  } else {
    //console.log('auth - no ip')
    processing(req, res, next);
  }
};