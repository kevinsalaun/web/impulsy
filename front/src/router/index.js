import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/gamemenu',
        name: 'GameMenu',
        component: () => import('../views/GameMenu.vue')
    },
    {
        path: '/hof',
        name: 'HoF',
        component: () => import('../views/HoF.vue')
    },
    {
        path: '/profil',
        name: 'Profil',
        component: () => import('../views/Profil.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router

/*
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/About.vue')
*/