// controller created for the future,
// currently unused

const express = require('express');
const blacklistService = require('./blacklist.service');

const router = express.Router();

// Set routes
router.delete('/', removeAll);
router.get('/ban/:ip', isBanned);
router.put('/ban/:ip', ban);
router.delete('/ban/:ip', unban);
module.exports = router;


// Set actions
/**
 * Returns true if the client's address is banned.
 * @route GET /ban/:ip
 * @group blacklist - Operations about blacklisting (router not connected)
 * @param {String} ip.query.required - ip to check
 * @returns {boolean} true | false.
 */
function isBanned(req, res, next) {
  blacklistService.isBanned(req.params.ip)
    .then(result => res.json(result))
    .catch(err => next(err));
}

/**
 * Bans an IP address.
 * @route PUT /ban/:ip
 * @group blacklist - Operations about blacklisting (router not connected)
 * @param {String} ip.query.required - ip to ban
 */
function ban(req, res, next) {
  blacklistService.ban(req.params.ip)
    .then(() => res.json({}))
    .catch(err => next(err));
}

/**
 * Unbans an IP address.
 * @route DELETE /ban/:ip
 * @group blacklist - Operations about blacklisting (router not connected)
 * @param {String} ip.query.required - ip to unban
 */
function unban(req, res, next) {
  blacklistService.unban(req.params.ip)
    .then(() => res.json({}))
    .catch(err => next(err));
}

/**
 * Unbans an IP address (for test only)
 * @route DELETE /
 * @group blacklist - Operations about blacklisting (router not connected)
 */
function removeAll(req, res, next) {
  blacklistService.removeAll()
    .then(() => res.json({}))
    .catch(err => next(err));
}
