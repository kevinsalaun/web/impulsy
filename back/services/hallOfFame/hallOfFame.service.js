﻿﻿const HallOfFame = require('./hallOfFame.model');
const Musics = require('../music/music.model');
const Users = require('../user/user.model');


async function getAllScores () {
  var hof = await HallOfFame.aggregate([
    { "$lookup": {
      "from": Users.collection.name,
      "localField": "idUser",
      "foreignField": "_id",
      "as": "user"
    }},
    { "$lookup": {
      "from": Musics.collection.name,
      "localField": "idMusic",
      "foreignField": "_id",
      "as": "music"
    }},
    { "$project": {
      "score": 1,
      "scoreDate": 1,
      "difficulty": 1,
      "gameMode": 1,
      "user.username": 1,
      "music.title": 1
    }}
  ])

  return hof;
}

async function getSpecificScores (data) {
  var filter = {}

  if(data.difficulty){
    filter.difficulty = data.difficulty
  }

  if(data.gameMode){
    filter.gameMode = data.gameMode
  }

  var hof = await HallOfFame.find(filter)
  return hof;
}

async function create(hofParam) {
  //Faire une vérification que l'utilisateur et la musique existent bien et si l'un des deux manque alors renvoyer un message d'erreur

  const score = new HallOfFame(hofParam);
  await score.save();
}

async function remove(id) {
  await HallOfFame.findByIdAndRemove(id);
}


module.exports = {
  getAllScores,
  getSpecificScores,
  create,
  remove
};
