import vue from 'vue'
import vuex from 'vuex'
import mutations from './mutation'
import getters from './getter'
import createPersistedState from 'vuex-persistedstate';

vue.use(vuex)

const state = {
    user: {
        auth: {
            userFingerprint: null,
            token: null,
            expire: null
        },
        info: {},
        settings: {}
    },
}

export default new vuex.Store({
    strict: true,
    state,
    getters,
    mutations,
    plugins: [createPersistedState()]
})
