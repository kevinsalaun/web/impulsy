/**
 * @typedef User
 * @property {string} username.required - username
 * @property {string} email.required - email
 * @property {string} password.required - password
 * @property {Date} birthday.required - birthday
 * @property {string} role - role, 'player' by default
 * @property {Date} createdDate - createdDate, Date.now by default
 */

const Storage = require('../storage');
const storage = new Storage().getInstance();

model = storage.model('User', {
  username: { type: String, unique: true, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  birthday: { type: Date, required: true },
  role: { type: String, default: 'player' },
  theme: { type: String, default: 'theme-default' },
  createdDate: { type: Date, default: Date.now }
});
  
module.exports = model;
