function makeToast(bvToast, title, msg, variant = null, duration = 5000) {
    bvToast.toast(msg, {
        title: title,
        variant: variant,
        autoHideDelay: duration,
        solid: true // no transparency
    });
}

function majorWarningAlert(
    createElement,
    bvToast,
    title,
    msg,
    duration = 5000
) {
    //this.$createElement    this.$bvToast
    const h = createElement;
    const vNodesTitle = h(
        'div',
        { class: ['d-flex', 'flex-grow-1', 'align-items-baseline'] },
        [
            h('div', {
                style: 'background-color: #ff5555; width: 12px; height: 12px',
                class: 'mr-2'
            }),
            h('strong', { class: 'mr-auto' }, title)
            //h('small', { class: 'text-muted mr-2' }, '5 minutes ago')
        ]
    );
    makeToast(bvToast, vNodesTitle, msg, 'warning', duration);
}

function warningAlert(createElement, bvToast, title, msg, duration = 5000) {
    //this.$createElement    this.$bvToast
    makeToast(bvToast, title, msg, 'warning', duration);
}

function infoAlert(createElement, bvToast, title, msg, duration = 5000) {
    //this.$createElement    this.$bvToast
    makeToast(bvToast, title, msg, 'info', duration);
}

function successAlert(createElement, bvToast, title, msg, duration = 5000) {
    //this.$createElement    this.$bvToast
    makeToast(bvToast, title, msg, 'success', duration);
}

function errorAlert(createElement, bvToast, title, msg, duration = 5000) {
    //this.$createElement    this.$bvToast
    makeToast(bvToast, title, msg, 'danger', duration);
}

function showError(createElement, bvToast, data) {
    if (data.title && data.msg) {
        warningAlert(createElement, bvToast, data.title, data.msg);
    } else {
        warningAlert(createElement, bvToast, 'Alert', data);
    }
}

module.exports = {
    makeToast,
    majorWarningAlert,
    warningAlert,
    infoAlert,
    successAlert,
    errorAlert,
    showError
};
