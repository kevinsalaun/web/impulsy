// numbering: 81 - 90

module.exports = {
    musicNotFound: {
        webCode: 404,
        refErr: 81,
    },
    missingMusic: {
        webCode: 400,
        refErr: 82,
    }
}