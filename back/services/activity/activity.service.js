﻿﻿const config = require('../../config');
const authErrorMsg = require('../../messages/auth.errors');
const Activity = require('./activity.model');
const blacklistService = require('../blacklist/blacklist.service');

async function record(ip) {
  // updates counters
  const record = await Activity.findOne({ ip: ip });
  if(record) {
    const reqLastMinute = (Date.now() - record.lastMinute <= 60000) ? record.reqLastMinute + 1 : 1;  // < 1m
    const lastMinute = (reqLastMinute == 1) ? Date.now() : record.lastMinute;
    //console.log('Date.now() - record.lastMinute <= 60000', Date.now() - record.lastMinute)
    //console.log('reqLastMinute', reqLastMinute)

    if(reqLastMinute > config.policy.maxReqByMinute) {
      // blacklist
      //console.log("activity record - request blacklisting");
      await blacklistService.ban(ip);
      const execState = await Activity.deleteOne({ ip: ip });
      //console.log("activity record remove "+ip+": "+execState.deletedCount);
      throw authErrorMsg.blacklisted;
    } else {
      // updates counters
      const execState = await Activity.updateOne({ ip: ip }, { 
        lastMinute: lastMinute,
        reqLastMinute: reqLastMinute, 
        $inc: { req: 1 } 
      }, {upsert: true});
      //console.log("activity record - added/updated "+ip+": "+execState.nModified+", found "+execState.n);
    }
  } else {
    const execState = await (new Activity({ ip: ip })).save();
    //console.log("activity create "+ip+": "+execState.createdDate);
  }
}

async function signal(ip) {
  if (! await blacklistService.isBanned(ip)) {
    const execState = await Activity.updateOne({ ip: ip }, { $inc: { unauthorizedActions: 1 } }, {upsert: true});
    //console.log("activity signal - added/updated "+ip+": "+execState.nModified+", found "+execState.n);
    const updatedRecord = await Activity.findOne({ ip: ip });
    if(updatedRecord.unauthorizedActions > config.policy.badDeedsThreshold) {
      //console.log("activity signal - request blacklisting");
      await blacklistService.ban(ip);
      const execState2 = await Activity.deleteOne({ ip: ip });
      //console.log("activity signal remove "+ip+": "+execState2.deletedCount);
      throw authErrorMsg.blacklisted;
    }
  }
}

async function removeAll() {
  const execState = await Activity.deleteMany({});  
  console.log("activity removeAll : "+execState.deletedCount+", found "+execState.n);
}

async function getActivity(ip) {
  const activity = await Activity.findOne({ ip: ip });
  return activity;
}

module.exports = {
  record,
  signal,
  getActivity,
  removeAll
};