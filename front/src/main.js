import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
//import VueSocketIO from 'vue-socket.io'
//import config from './assets/config'

// Indexing of supported languages
import languages from '@/assets/languages/languages';

// Set the following to `true` to hide the logs
Vue.config.productionTip = false;

// Socket.io
/*Vue.use(new VueSocketIO({
    debug: false, //true
    connection: config.backApi.uri,
    options: {
        reconnectionAttempts: 5
    },
    vuex: {
        store,
        actionPrefix: "SOCKET_",
        mutationPrefix: "SOCKET_",
    }
}));*/

new Vue({
    i18n: languages,
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app');
