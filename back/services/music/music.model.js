const Storage = require('../storage');
const storage = new Storage().getInstance();

model = storage.model('Musics', {
  title:    { type: String, required: true },
  album:    { type: String },
  artists:  { type: String },
  duration: { type: String, required: true },
  genres:   { type: Object },
  cover:    { type: String },
  spectrum: { type: Object, required: true },
  gamemode: { type: String, enum: ['classic', 'brise', 'drummer'], required: true }
});
  
module.exports = model;
