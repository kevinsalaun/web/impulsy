const express = require('express');
const hofService = require('./hallOfFame.service');

const router = express.Router();

// Set routes
router.get('/', getAllScores);
router.post('/', getSpecificScores);
router.put('/', create);
router.delete('/:id', remove);
module.exports = router;

// Set actions
function getAllScores(req, res, next) {
  hofService.getAllScores()
    .then(hof => res.json(hof))
    .catch(err => next(err));
}

function getSpecificScores(req, res, next){
  hofService.getSpecificScores(req.body)
    .then(hof => res.json(hof))
    .catch(err => next(err));
}

function create(req, res, next) {
  hofService.create(req.body)
    .then(() => res.json({}))
    .catch(err => next(err));
}

function remove(req, res, next) {
  hofService.remove(req.params.id)
    .then(() => res.json({}))
    .catch(err => next(err));
}