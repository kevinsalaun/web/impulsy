// numbering: 91 - 99

module.exports = {
  gameRoomNotFound: {
    webCode: 400,
    refErr: 91,
  },
  unauthorized: {
    webCode: 400,
    refErr: 92,
  },
  notTheOwner: {
    webCode: 400,
    refErr: 93,
  },
  invalidInput: {
    webCode: 400,
    refErr: 94,
  },
  blacklisted: {
    webCode: 403,
    refErr: 95,
  },
  playerNotFound: {
    webCode: 400,
    refErr: 96,
  },
  playlistAlreadyLocked: {
    webCode: 400,
    refErr: 97,
  },
}