function listeners(connection, socket) {
  var userGames = []

  /******************** A virer une fois une solution plus stable codée ************************/
  var player1Taken = false;
  var player2Taken = false;
  /*********************************************************************************************/

  // A new client has come online. Check the secret key and 
  // emit a "granted" or "denied" message.
  /*socket.on('load', function(data){
    game = userGames.filter(p => p.serial == data.serial)

    if(typeof game == "undefined" || game == null || game.length == null || game.length == 0){
      userGames.push({'serial':data.serial, 'key':data.key})
      console.log(userGames)
    }

    socket.emit('access', {
      access: (data.key === game.key ? "granted" : "denied")
    });
  });*/

  socket.on('playerChosen', function(data, fn){
    if(data.player == 1 && !player1Taken){
      player1Taken = true
      fn({taken:false})
    }
    else if(data.player == 2 && !player2Taken){
      player2Taken = true
      fn({taken:false})
    }
    else{
      fn({taken:true})
    }
  })

  socket.on('jump', function(data){
    connection.emit('jump', {
      player: data.player
    });
  })

  socket.on('draw', function(data){
    connection.emit('draw', {
      player: data.player
    });
  })

  socket.on('clear', function(data){
    connection.emit('clear', {
      player: data.player
    });
  })
}

module.exports = {
  listeners,
};
