const express = require('express');
const musicService = require('./music.service');
const multiparty = require('multiparty');

const router = express.Router();

// Set routes
router.get('/', getAllMusics);
router.get('/:id', getOneMusic);
router.put('/', create);
router.delete('/:id', remove);
router.post('/:id', update)
router.delete('/', removeAll);
module.exports = router;

// Set actions
function getAllMusics(req, res, next) {
  musicService.getAllMusics()
    .then(music => res.json(music))
    .catch(err => next(err));
}

function getOneMusic(req, res, next){
  musicService.getSpecificMusic(req.params.id)
    .then(music => res.json(music))
    .catch(err => next(err));
}

function create(req, res, next) {
  const musicLocation = 'services/music/tmp/';
  var form = new multiparty.Form({
    uploadDir: musicLocation
  });
  form.parse(req, function(err, fields, files) {
    if (err) { next(err); return; }
    musicService.create(fields, files, musicLocation)
      .then((data) => res.json(data))
      .catch(err => next(err));
  });  
}

function remove(req, res, next) {
  musicService.remove(req.params.id)
    .then(() => res.json({}))
    .catch(err => next(err));
}

function update(req, res, next) {
  musicService.update(req.params.id, req.body)
    .then(() => res.json({}))
    .catch(err => next(err));
}

function removeAll(req, res, next) {
  musicService.removeAll()
    .then(() => res.json({}))
    .catch(err => next(err));
}