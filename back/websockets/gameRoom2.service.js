// Redis database (caching)
let RedisClient = require('../redis');
let redisClient = new RedisClient().getInstance();
let UserDBService = require('../services/user/user.service');

function checkRoomExists(io, roomId) {
    // check that the room exists
    let room = io.sockets.adapter.rooms[roomId];
    if (room == undefined) throw 'room doesn\'t exist';
}

async function checkIsOwner(userId, roomId) {
    // check that the user is the room owner
    let owner = await redisClient.readFirst(`${roomId}.owner`);
    if (owner != userId) throw 'user is not the room owner';
}

async function checkPlayerInRoom(userId, roomId) {
    // check that the user is a player of the room
    if (
        !await redisClient.readFirst(`${roomId}.playerlist.allinones.${userId}`) && 
        !await redisClient.readFirst(`${roomId}.playerlist.controllers.${userId}`) && 
        !await redisClient.readFirst(`${roomId}.playerlist.screens.${userId}`)
    ) {
        throw 'user is not the room owner';
    }
}

async function checkRoomNotLocked(roomId) {
    // check that the room is not locked
    let locked = await redisClient.readFirst(`${roomId}.locked`);
    if (locked) throw 'room locked';
}

/* *******************************
    *       HOST FUNCTIONS         *
    ******************************* */
async function hostCreateNewGame(userFingerprint, settings, socket) {
    try {
        // create a unique id
        var roomId = ( Math.random() * 100000 ) | 0;

        // get player information
        let userId = userFingerprint.id;
        let info = await UserDBService.getById(userId);
        let ownerInfo = {
            sid: socket.id,
            isscreen: false,
            screenDescription: null,
            username: info.username,
            avatar: info.avatar,
            statusLbl: 'ready',
            status: 3
        };

        // save owner info
        await redisClient.write(`${roomId}.owner`, userId);
        //await redisClient.write(`${roomId}.ownerSID`, socket.id);
        await redisClient.writeJson(`${roomId}.playerlist.allinones.${userId}`, ownerInfo);
        await redisClient.write(`socketAttached.${socket.id}`, userId);

        // save game settings in redis
        await redisClient.writeJson(`${roomId}.settings`, settings);

        // join the room and wait for the players
        socket.join(roomId.toString());

        // return the game room id
        return {
            id: roomId
        };
    } catch (err) {
        throw err
    }
};

async function hostAddMedia(io, userFingerprint, roomId, mediaData) {
    try {
        let userId = userFingerprint.id;
        checkRoomExists(io, roomId);
        await checkIsOwner(userId, roomId)
        await checkRoomNotLocked(roomId);

        // create a unique id
        var mediaId = ( Math.random() * 100000 ) | 0;

        // add the media in redis
        await redisClient.writeJson(`${roomId}.playlist.${mediaId}`, mediaData);

        // broadcast information into the game room
        io.sockets.in(roomId).emit('addMedia', { id: mediaId, data: mediaData });

        return {};
    } catch (err) {
        throw err
    }
};

async function hostDelMedia(io, userFingerprint, roomId, mediaId) {
    try {
        let userId = userFingerprint.id;
        checkRoomExists(io, roomId);
        await checkIsOwner(userId, roomId)
        await checkRoomNotLocked(roomId);

        // del the media from redis
        await redisClient.del(`${roomId}.playlist.${mediaId}`);

        // broadcast information into the game room
        io.sockets.in(roomId).emit('delMedia', mediaId);

        return {};
    } catch (err) {
        throw err
    }
};

async function hostKickPlayer(io, userFingerprint, roomId, playerId) {
    try {
        let userId = userFingerprint.id;
        checkRoomExists(io, roomId);
        await checkIsOwner(userId, roomId)
        await checkRoomNotLocked(roomId);

        // del the player from redis
        await redisClient.del(`${roomId}.playerlist.allinones.${playerId}`);
        await redisClient.del(`${roomId}.playerlist.controllers.${playerId}`);
        await redisClient.del(`${roomId}.playerlist.screens.${playerId}`);

        // broadcast information into the game room
        io.sockets.in(roomId).emit('playerKicked', playerId);

        return {};
    } catch (err) {
        throw err
    }
};

async function hostBanPlayer(io, userFingerprint, roomId, playerId) {
    try {
        let userId = userFingerprint.id;
        checkRoomExists(io, roomId);
        await checkIsOwner(userId, roomId)
        await checkRoomNotLocked(roomId);

        // blacklist the player in redis
        await redisClient.write(`${roomId}.blacklist.${playerId}`, true);

        // del the player from redis
        await redisClient.del(`${roomId}.playerlist.allinones.${playerId}`);
        await redisClient.del(`${roomId}.playerlist.controllers.${playerId}`);
        await redisClient.del(`${roomId}.playerlist.screens.${playerId}`);

        // broadcast information into the game room
        io.sockets.in(roomId).emit('playerBanned', playerId);

        return {};
    } catch (err) {
        throw err
    }
}

async function hostPrepareGame(io, userFingerprint, roomId) { //go to game Vue
    try {
        let userId = userFingerprint.id;
        checkRoomExists(io, roomId);
        await checkIsOwner(userId, roomId)
        await checkRoomNotLocked(roomId);

        await redisClient.write(`${roomId}.locked`, true);

        // TODO: treatment
        // ...

        // broadcast information into the game room
        io.sockets.in(roomId).emit('gameStart');

        return {};
    } catch (err) {
        throw err
    }
}

async function hostNextTrack(io, userFingerprint, roomId) { // get the next track and sync clients with a countdown
    try {
        let userId = userFingerprint.id;
        checkRoomExists(io, roomId);
        await checkIsOwner(userId, roomId)

        // TODO: treatment
        // ...

        return {};
    } catch (err) {
        throw err
    }
}


/* *****************************
    *     PLAYER FUNCTIONS        *
    ***************************** */
async function playerJoinGame(io, userFingerprint, roomId, isScreen, screenDescription, socket) {
    try {
        let userId = userFingerprint.id;
        checkRoomExists(io, roomId);
        await checkRoomNotLocked(roomId);
        
        // check if player banned
        let banned = await redisClient.readFirst(`${roomId}.blacklist.${userId}`);
        if (banned) throw 'player is banned'

        if(
            await redisClient.readFirst(`${roomId}.owner`) == userId || 
            await redisClient.readFirst(`${roomId}.playerlist.allinones.${userId}`) || 
            await redisClient.readFirst(`${roomId}.playerlist.controllers.${userId}`) || 
            await redisClient.readFirst(`${roomId}.playerlist.screens.${userId}`)
        ) {
            // only re-attache the socket if user already in the room
            console.log('re-attache the socket');
            socket.join(roomId.toString());
        } else {
            // get player information
            let info = await UserDBService.getById(userId);
            let playerInfo = {
                id: userId,
                sid: socket.id,
                isscreen: isScreen,
                screenDescription: screenDescription,
                username: info.username,
                avatar: info.avatar,
                statusLbl: 'ready',
                status: 3
                //statusLbl: 'connection',
                //status: 2
            };

            // add player in redis
            if (isScreen) {
                await redisClient.writeJson(`${roomId}.playerlist.screens.${userId}`, playerInfo);
            } else {
                await redisClient.writeJson(`${roomId}.playerlist.allinones.${userId}`, playerInfo);
            }
            await redisClient.write(`socketAttached.${socket.id}`, userId);
            
            //await redisClient.write(`${roomId}.playerlist.${socket.id}`, true);

            // join the room
            socket.join(roomId.toString());

            // broadcast information into the game room
            io.sockets.in(roomId).emit('playerJoined', { id: userId, data: playerInfo });
        }

        return {};
    } catch (err) {
        throw err
    }
};


    /* *****************************
    *             ALL             *
    ***************************** */
async function gameSummary(io, userFingerprint, roomId, socket) {
    try {
        let userId = userFingerprint.id;
        checkRoomExists(io, roomId);
        await checkPlayerInRoom(userId, roomId);
        await checkRoomNotLocked(roomId);

        // if not in the game room, join it
        let isScreen = await redisClient.readJsonFirst(`${roomId}.playerlist.screens.${userId}`);
        let screenDescription = (isScreen) ? isScreen.screenDescription : null;
        playerJoinGame(io, userFingerprint, roomId, (isScreen != undefined), screenDescription, socket);

        // get info from redis
        let settings = await redisClient.readJsonFirst(`${roomId}.settings`);
        let owner = await redisClient.readFirst(`${roomId}.owner`);
        //let owner = await redisClient.readFirst(`${roomId}.owner`);
        let playerlist = {
            ...await redisClient.readJson(`${roomId}.playerlist.allinones`),
            ...await redisClient.readJson(`${roomId}.playerlist.controllers`)
        };
        let screens = await redisClient.readJson(`${roomId}.playerlist.screens`);
        let playlist = await redisClient.readJson(`${roomId}.playlist`);

        //console.log('settings:', settings);
        let result = {
            name: settings.roomname,
            owner: owner,
            multiplayer: settings.multiplayer,
            playerLimit: settings.playerLimit,
            playerlist: playerlist,
            screens: screens,
            playlist: playlist
        };
        
        // call callback without error
        return {
            ...result
        };
    } catch (err) {
        throw err;
    }
};

async function attachScreen(io, userFingerprint, roomId, playerId, screenId) {
    try {
        checkRoomExists(io, roomId);
        await checkRoomNotLocked(roomId);

        // check that the screen exists
        let screen = await redisClient.readJson(`${roomId}.playerlist.screens.${screenId}`);
        if (screen == undefined) throw 'screen doesn\'t exist';

        let playerInfo = await redisClient.readJson(`${roomId}.playerlist.allinones.${playerId}`);
        if (playerInfo) {
            // first attach
            playerInfo.screen = screenId;
            await redisClient.del(`${roomId}.playerlist.allinones.${playerId}`);
            await redisClient.writeJson(`${roomId}.playerlist.controllers.${playerId}`, playerInfo);
        } else {
            // change attach
            playerInfo = await redisClient.readJson(`${roomId}.playerlist.controllers.${playerId}`);
            if(playerInfo) {
                playerInfo.screen = screenId;
                await redisClient.writeJson(`${roomId}.playerlist.controllers.${playerId}`, playerInfo);
            } else {
                throw 'client doesn\'t exist';
            }
        }

        // broadcast information into the game room
        io.sockets.in(roomId).emit('playerUpdated', { id: userId, data: playerInfo });

        return {};
    } catch (err) {
        throw err
    }
};

async function detachScreen(io, userFingerprint, roomId, playerId) {
    try {
        checkRoomExists(io, roomId);
        await checkRoomNotLocked(roomId);

        let playerInfo = await redisClient.readJson(`${roomId}.playerlist.controllers.${playerId}`);
        if (!playerInfo) throw 'client doesn\'t exist';

        delete playerInfo.screen;
        await redisClient.del(`${roomId}.playerlist.controllers.${playerId}`);
        await redisClient.writeJson(`${roomId}.playerlist.allinones.${playerId}`, playerInfo);

        // broadcast information into the game room
        io.sockets.in(roomId).emit('playerUpdated', { id: userId, data: playerInfo });

        return {};
    } catch (err) {
        throw err
    }
}

async function leaveGame(io, userFingerprint, roomId, socket) {
    try {
        checkRoomExists(io, roomId);
        
        let userId = userFingerprint.id;
        let playerlist = {
            ...await redisClient.readJson(`${roomId}.playerlist.allinones`),
            ...await redisClient.readJson(`${roomId}.playerlist.controllers`)
        }

        if (await redisClient.readFirst(`${roomId}.owner`) == userId) {
            // owner case - new owner election
            let keys = Object.keys(playerlist);
            if (keys.length == 0) {
                // remove the game room
                await redisClient.del(`${roomId}*`);
            } else {
                // save the new owner
                let newOwnerId = keys[Math.floor(Math.random() * keys.length)];
                await redisClient.write(`${roomId}.owner`, newOwnerId);
            }

        } else if (await redisClient.readFirst(`${roomId}.playerlist.screens.${userId}`)) {
            // screen case
            await redisClient.del(`${roomId}.playerlist.screens.${userId}`);

            // convert controller to allinone
            let playerlist = await redisClient.readJson(`${roomId}.playerlist.controllers`);
            for (let idx in playerlist) {
                let playerKey = playerlist[idx];
                if (playerKey.screen == userId) {
                    let playerInfo = await redisClient.readJsonFirst(`${roomId}.playerlist.controllers.${playerKey}`);
                    await redisClient.del(`${roomId}.playerlist.controllers.${playerKey}`);
                    delete player.screen;
                    await redisClient.writeJson(`${roomId}.playerlist.allinones.${playerKey}`, playerInfo);
                }
            }
        } else {
            await redisClient.del(`${roomId}.playerlist.allinones.${userId}`);
            await redisClient.del(`${roomId}.playerlist.controllers.${userId}`);
        }
        await redisClient.del(`socketAttached.${socket.id}`);

        // broadcast information into the game room
        io.sockets.in(roomId).emit('playerLeaved', userId);

        return {};
    } catch (err) {
        throw err
    }
}

async function leaveGameFromSocket(io, roomId, socket) {
    try {
        let userFingerprint = {
            id: await redisClient.readFirst(`socketAttached.${socket.id}`)
        };
        await leaveGame(io, userFingerprint, roomId, socket);
    } catch (err) {
        throw err
    }
}

async function publicRoomList(userFingerprint) {
    try {
        let roomSettings = await redisClient.readJson('.settings');
        console.log('roomSettings:', roomSettings);

        return {
            roomSettings
        };
    } catch (err) {
        throw err
    }
}

async function flush() {
    try {
        await redisClient.delAll();
    } catch (err) {
        throw err
    }
}


module.exports = {
    hostCreateNewGame,
    hostAddMedia,
    hostDelMedia,
    hostKickPlayer,
    hostBanPlayer,
    hostPrepareGame,
    hostNextTrack,
    /*hostStartGame,*/

    // Player Events
    playerJoinGame,
    /*playerMove,*/

    // All Events
    gameSummary,
    attachScreen,
    detachScreen,
    leaveGame,
    leaveGameFromSocket,
    publicRoomList,

    flush
};