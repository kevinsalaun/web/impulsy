export default {
    isAuth(state) {
        /*try {
            if (state.user.auth.expire) {
                const expire = new Date(state.user.auth.expire);
                return (expire.getMilliseconds() < new Date().getMilliseconds());
            } else {
                console.log('isAuth: expire not set');
                return false;
            }
        } catch(error) {
            console.log('isAuth - err:', error);
            return false;
        }*/
        return (state.expire && new Date(state.expire).getTime() > new Date().getTime()) ? true : false;
    },

    //userFingerprint(state) { return state.user.auth.userFingerprint },

    //auth: (state) => state.userList.map(state.user.auth),
    auth(state) { 
        try {
            if(state.user.auth) {
                return state.user.auth;
            }
        } catch(error) {
            return null;
        }
    },

    /*id(state) { 
        try {
            if(state.user.auth.userFingerprint.id) {
                return state.user.auth.userFingerprint.id;
            }
        } catch(error) {
            return null;
        }
    },*/

    info(state) { 
        try {
            if(state.user.info) {
                return state.user.info;
            }
        } catch(error) {
            return null;
        }
    },

    settings(state) { 
        try {
            if(state.user.settings) {
                return state.user.settings;
            }
        } catch(error) {
            return {};
        }
    },

    theme(state) {
        return (state.user.settings.theme) ? (state.user.settings.theme) : 'theme-default';
    },

    data(state) { 
        return state 
    }
}