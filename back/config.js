//const userErrorMsg = require('./messages/user.errors');
const authErrorMsg = require("./messages/auth.errors");

config = {
  app: {
    production: false,
    protocol: "http",
    uri: "backend-impulsy.local",
    port: 8085,
    defaultLanguage: "fr"
  },
  front: {
    uri: "http://frontend-impulsy.local:8080"
  },
  db: {
    uri: "mongodb://localhost:27017",
    database: "impulsy",
    tokenSecret: "s3cret"
  },
  policy: {
    hash: {
      saltRounds: 10
    },
    token: {
      expire: "60m"
    },
    maxReqByMinute: 40,
    badDeedsThreshold: 10,
    badDeedList: [authErrorMsg.attemptViolationOtherUser.refErr]
  }
};

module.exports = config;
