'use strict';

//const createError  = require('http-errors');
const express        = require('express');
const cors           = require('cors');
//const path         = require('path');
//const cookieParser = require('cookie-parser');
const logger         = require('morgan');
const bodyParser     = require('body-parser');
//const favicon      = require('serve-favicon');
const swaggerJSDoc   = require('swagger-jsdoc');
const swaggerUi      = require('swagger-ui-express');
const fs             = require('fs');
const config         = require('./config');


// Connect the storage
const Storage = require('./services/storage');
new Storage().getInstance();

// Load middlewares
const activityService = require('./services/activity/activity.service');
const gameRoomService = require('./services/gameRoom/gameRoom.service');
const statistics = require('./middleware/statistics');
const auth = require('./middleware/auth');
const WebSocket = require('./websockets/websocket');

// Load routers
//const indexRouter = require('./routes/index');
const hofRouter = require('./services/hallOfFame/hallOfFame.controller');
const musicRouter = require('./services/music/music.controller');
const userRouter = require('./services/user/user.controller');
const gameRoomRouter = require('./services/gameRoom/gameRoom.controller');

// Creation of the express app
const app = express();

// Swagger configuration (documentation)
//// Swagger definition
const packageInfo = JSON.parse(fs.readFileSync('package.json'));
const swaggerDefinition = {
  info: {
    description: packageInfo.description,
    title: packageInfo.name,
    version: packageInfo.version,
  },
  host: config.app.uri+':'+config.app.port, // the host or url of the app
  basePath: '/', // the basepath of your endpoint
}

//// options for the swagger docs
const options = {
  swaggerDefinition,
  apis: ['./swagger-doc/*.yml'], // path to the API docs
};

//// initialize swagger-jsdoc and use swagger-Ui-express
const swaggerSpec = swaggerJSDoc(options);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// Configure the CORS (Cross-Origin Resource Sharing)
var corsOptions = {
  origin: config.front.uri,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.use(cors(corsOptions));

// Preparation of the express app
var logFolder = './logs'
if (!fs.existsSync(logFolder)) {
  fs.mkdirSync(logFolder, { recursive: true })
}
app.use(logger('dev', {
  stream: fs.createWriteStream(logFolder+'/access.log', {flags: 'a'})
}));
if(app.get("env") === "development") {
  app.use(logger('dev'));
} else {
  console.log = function() {}
}

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
//app.use(cookieParser());

// Attache routers
app.use('/hallOfFame', hofRouter);
app.use('/music', musicRouter);
app.use(statistics);
app.use(auth);
app.use('/gameRoom', gameRoomRouter);
app.use('/user', userRouter);

// Catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.status(404).send('This path does not exist');
});

// Error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.refErr;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // updates unauthorized action counter
  if(config.policy.badDeedList.includes(err.refErr)) {
    console.log('Ho dude, you are a bad boy...')
    activityService.signal(req.connection.remoteAddress)
      .then(() => {
        render();
      })
      .catch((error) => {
        err = error;
        render();
      })
  } else {
    render();
  }

  function render() {
    // render the error information
    console.log(err)
    res.status(err.webCode || 500).json({error: err.refErr});
  }
});

// Reset gamerooms
gameRoomService.removeAll();

// Server launching
const server = require('http').Server(app);
const websocket = new WebSocket().getInstance();
websocket.init(server);
server.listen(config.app.port, () =>
  console.log(`The API is listening on port ${config.app.port}!`),
);
