﻿﻿const ms = require('ms');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../../config');
const userErrorMsg = require('../../messages/user.errors');
const User = require('./user.model');
const roleEnum = require('../role');

async function create(userParam) {
  // validate
  compliantUserForm(userParam);
  if (await User.findOne({ username: userParam.username })) {
    throw userErrorMsg.userAlreadyExists;
  }
  if (await User.findOne({ email: userParam.email })) {
    throw userErrorMsg.emailAlreadyUsed;
  }

  const user = new User(userParam);

  // hash password
  if (userParam.password) {
    user.password = bcrypt.hashSync(userParam.password, config.policy.hash.saltRounds);
  }

  // save user
  const execState = await user.save();
  console.log("user create "+userParam.username+": "+execState.createdDate);

  // connect the user
  return await authenticate({
    email: userParam.email, 
    password: userParam.password
  });
}

async function authenticate(userParam) {
  var filter;
  if(userParam.username != undefined) {
    filter = { username: userParam.username };
  } else if(userParam.email != undefined) {
    filter = { email: userParam.email };
  }
  if(filter == undefined) {
    throw userErrorMsg.missingUserLogin;
  }

  const userData = await User.findOne(filter);
  
  if(!userData) {
    throw userErrorMsg.badLogin;
  }
  const hashedPassword = userData.password;
  const userFingerprint = {
    id: userData.id,
    username: userData.username,
    email: userData.email
  }

  if(!bcrypt.compareSync(userParam.password, hashedPassword)) {
    throw userErrorMsg.badLogin;
  }

  const token = await jwt.sign(userFingerprint, config.db.tokenSecret, { expiresIn: config.policy.token.expire });
  if (!token) throw userErrorMsg.tokenGenerationError;
  const expire = new Date(new Date().getTime() + ms(config.policy.token.expire))

  return {
    userFingerprint,
    token: token,
    expire: expire,
    info: {
      birthday: userData.birthday
    },
    settings: {
      theme: userData.theme
    }
  };
}

async function update(id, userParam) {
  console.log('it\'s an update')
  // checks the existence of the user and loads his/her data
  const oldUser = await User.findById(id);
  if (!oldUser) throw userErrorMsg.userNotFound;

  // merges old and new user information
  var newUser = {};
  Object.assign(newUser, oldUser.toJSON());
  Object.assign(newUser, userParam);
  delete newUser.id;
  delete newUser._id;
  delete newUser.createdDate;
  delete newUser.__v;

  if (!userParam.password) {
    delete newUser.password
  }
  console.log('newUser:', newUser);
  
  // checks the conformity of the data provided
  compliantUserForm(newUser, true);
  
  // hash password
  if (userParam.password) {
    newUser.password = bcrypt.hashSync(newUser.password, config.policy.hash.saltRounds);
  }
  if (oldUser.username !== newUser.username && await User.findOne({ username: newUser.username })) {
    throw userErrorMsg.userAlreadyExists;
  }
  if (oldUser.email !== newUser.email && await User.findOne({ email: newUser.email })) {
    throw userErrorMsg.emailAlreadyUsed;
  }  

  // save user
  const execState = await User.updateOne({ _id: id }, newUser);
  console.log("user updated "+id+": "+execState.nModified+", found "+execState.n);

  return {
    info: {
      birthday: newUser.birthday
    },
    settings: {
      theme: newUser.theme
    }
  };
}

async function remove(id) {
  const execState = await User.deleteOne({ _id: id });
  console.log("user remove "+id+": "+execState.deletedCount);
}

async function removeAll() {
  const execState = await User.deleteMany({});  
  console.log("user removeAll : "+execState.deletedCount+", found "+execState.n);
}

async function getCurrent(id) {
  const user = await User.findById(id).select('-id -_id -password');
  if (!user) throw userErrorMsg.userNotFound;
  return user;
}

async function getById(id) {
  const user = await User.findById(id).select('-id -_id -password');
  if (!user) throw userErrorMsg.userNotFound;
  return user;
}

function compliantUserForm(userParam, update=false) {
  console.log('beginning of compliantUserForm');
  
  //Password policy
  /*
  1 lowercase character            (?=.*[a-z])
  1 uppercase character            (?=.*[A-Z])
  1 numeric character              (?=.*[0-9])
  1 special character              (?=.[!@#\$%\^&\*])
  */
 
  //console.log('userParam', userParam);
  
  if(!update) {
    if (userParam == null) {
      throw userErrorMsg.incompleteForm;
    } else {
      if (
        userParam.username == null || 
        userParam.email == null ||  
        userParam.password == null ||
        userParam.birthday == null
      ) {
        throw userErrorMsg.incompleteForm;
      }
    }
  }

  if(userParam.username) {
    if(
      userParam.username.length > 12 ||
      userParam.username.length < 4 ||
      !userParam.username.match(/^[A-Za-z0-9]+$/)
    ) {
      throw userErrorMsg.usernamePolicyWarning;
    }
  }

  if(userParam.email) {
    if(
      !userParam.email.match(/^[\w.-]+@\w(?:[\w-]{0,61}\w)?(?:\.\w(?:[\w-]{0,61}\w)?)*$/)
    ) {
      throw userErrorMsg.emailPolicyWarning;
    }
  }

  if(userParam.password) {
    console.log('password:', userParam.password);
    
    if(
      userParam.password.length > 25 ||
      userParam.password.length < 8 ||
      !userParam.password.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*_-])/)
    ) {
      throw userErrorMsg.passwordPolicyWarning;
    }
  }

  if(userParam.birthday) {
    if(!userParam.birthday instanceof Date) {
      throw userErrorMsg.birthdayDateFormat;
    }
  }

  if(userParam.role) {
    if(!roleEnum[userParam.role]) {
      throw userErrorMsg.nonexistentRole;
    }
  }

  console.log('end of compliantUserForm');
}

module.exports = {
  create,
  authenticate,
  update,
  remove,
  getCurrent,
  getById,
  removeAll
};
