/**
 * @typedef Blacklist
 * @property {string} ip.required - ip
 * @property {Date} createdDate - createdDate, Date.now by default
 * @property {Date} expireAt - expireAt, Date.now by default (the record is deleted upon expiry - 7d)
 */

const Storage = require('../storage');
const storage = new Storage().getInstance();

model = storage.model('Blacklist', {
  ip: { type: String, unique: true, required: true },
  createdDate: { type: Date, default: Date.now },
  expireAt: { type: Date, default: Date.now, index: { expires: '7d' } },
});
  
module.exports = model;
