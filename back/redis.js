class RedisClient {
  constructor() {
    let Redis = require('ioredis');
    this.redis = new Redis();
    /*let JSONCache = require('redis-json');
    this.jsonCache = new JSONCache(this.redis, {prefix: 'cache:'});*/
  }

  json2String(json) {
    return JSON.stringify(json);
  }

  string2Json(str) {
    return JSON.parse(str);
  }
  
  readFirst(key) {
    return new Promise((resolve, reject) => {
      this.redis.get(key)
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err.toString());
        });
    });
  }

  readJsonFirst(key) {
    return new Promise((resolve, reject) => {
      this.readFirst(key)
        .then((data) => {
          resolve(this.string2Json(data));
        })
        .catch((err) => {
          reject(err.toString());
        });
    });
  }

  read(pattern) {
    return new Promise((resolve, reject) => {
      var keyList = [];
      let indexList = [];
      let promiseList = [];
      var res = {};

      let stream = this.redis.scanStream({
        match: pattern,
        count: 100  // max 100 elements per call
      });
      stream.on('data', (result) => {
        keyList = keyList.concat(result);
      });

      stream.on('end', () => {
        // remove keyList duplicate values
        keyList = Array.from(new Set(keyList));
        console.log('keyList:', keyList);

        // for each key get the value
        for (var i=0; i<keyList.length; i++) {
          let key = keyList[i];
          indexList.push(key);
          console.log('key:', key);
          promiseList.push(this.readFirst(key));
        }
        console.log('promiseList:', promiseList);

        // wait promises and concat their results into a JSON
        Promise.all(promiseList)
          .then((values) => {
            for (var i=0; i<indexList.length; i++) {
              let key = indexList[i];
              res[key] = values[i];
            }
            console.log('res:', res);
            resolve(res);
          });
      });

      stream.on('error', (err) => {
        reject(err);
      })
    });
  }

  readJson(key) {
    return new Promise((resolve, reject) => {
      this.read(`*${key}*`)
        .then((data) => {
          let newData = {};
          let pattern = `${key}`;
          for (let idx in data) {
            console.log('DEBUG:', idx);
            let shortIdx = idx.substring(idx.indexOf(pattern)+pattern.length)
            console.log('DEBUG2:', shortIdx);
            newData[shortIdx] = this.string2Json(data[idx]); //////////////gérer le cas ou idx vide après replacement
          }
          resolve(newData);
        })
        .catch((err) => {
          reject(err.toString());
        });
    });
  }

  write(key, value, expire=null) {
    return new Promise((resolve, reject) => {
      if(expire) {
        this.redis.set(key, value, 'ex', expire)
          .then((data) => {
            resolve(data);
          })
          .catch((err) => {
            reject(err.toString());
          });
      } else {
        this.redis.set(key, value)
          .then((data) => {
            resolve(data);
          })
          .catch(() => {
            reject(err.toString());
          });
      }
    });
  }

  writeJson(key, value, expire=null) {
    return new Promise((resolve, reject) => {
      this.write(key, this.json2String(value), expire)
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err.toString());
        });
    });
  }

  del(key) {
    return new Promise((resolve, reject) => {
      this.redis.del(key)
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err.toString());
        });
    });
  }

  delAll() {
    return new Promise((resolve, reject) => {
      this.redis.flushdb()
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err.toString());
        });
    });
  }
}

class Singleton {
  constructor() {
    if (!Singleton.instance) {
      Singleton.instance = new RedisClient();
    }
  }

  getInstance() {
    return Singleton.instance;
  }
}

module.exports = Singleton;