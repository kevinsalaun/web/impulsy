#!/bin/python
from scipy.io import wavfile
import numpy as np
import sys
import json
import math
import pydub
import warnings
warnings.filterwarnings("ignore")

def groupedAvg(myArray, N=2):
  result = np.cumsum(myArray, 0)[N-1::N]/float(N)
  result[1:] = result[1:] - result[:-1]
  return result
  

def mp3read(f, normalized=False):
  """MP3 to numpy array"""
  a = pydub.AudioSegment.from_mp3(f)
  y = np.array(a.get_array_of_samples())
  if a.channels == 2:
    y = y.reshape((-1, 2))
  if normalized:
    return a.frame_rate, np.float32(y) / 2**15
  else:
    return a.frame_rate, y


def main():
  res = {}
  
  file_location = sys.argv[1]
  if file_location[-1:] != '/':
    file_location = file_location + '/'
  
  for arg in sys.argv[2:]: 
    # Loads wav file as array.
    file_name = arg
    extension = file_name.split('.')[-1]
    rate, data = (None, None)
    if (extension == 'mp3'):
      rate, data = mp3read(file_location + file_name)
    else:
      rate, data = wavfile.read(file_location + file_name)

    # Get global information
    sound_length_sec = data.shape[0] / rate
    sound_quality = data.dtype
  
    # Dissociates the channels (sound mono or stereo)
    channel1 = None
    channel2 = None
    if len(data.shape) == 1:
      channel1 = data
      channel2 = np.empty(channel1.shape)
    else:
      channel1 = data[:,0] # left
      channel2 = data[:,1] # right

    # Spectrum degradation
    desired_slice_per_sec = 4
    #
    slice_duration = sound_length_sec /channel1.shape[0]
    coef_selector = round(1/(desired_slice_per_sec*slice_duration))

    ## To be used for the generation of the map
    channel1_deg = groupedAvg(channel1, coef_selector)
    channel2_deg = groupedAvg(channel2, coef_selector)

    """
    row_count = math.ceil(channel1.shape[0]/coef_selector)
    channel_deg_shape = (row_count, 1)
    channel1_deg = np.empty(channel_deg_shape)
    channel2_deg = np.empty(channel_deg_shape)

    j = -1
    for i in range(0, channel1.shape[0]):
      if i % coef_selector  == 0:
        j += 1
        channel1_deg[j] = channel1[i]
        channel2_deg[j] = channel2[i]
      else:
        channel1_deg[j] += (channel1[i] / coef_selector)
        channel2_deg[j] += (channel2[i] / coef_selector)
    """

    # print data
    res_channel1 = channel1_deg.flatten().tolist()
    res_channel2 = channel2_deg.flatten().tolist()

    res[file_name] = {
      #  "quality": sound_quality,
      "length": sound_length_sec,
      "rate": rate,
      "slice_duration": slice_duration*coef_selector,
      "channels_data": {
        "channel1": {
          "min": min(res_channel1),
          "max": max(res_channel1),
          "data": res_channel1
        },
        "channel2": {
          "min": min(res_channel2),
          "max": max(res_channel2),
          "data": res_channel2
        }
      }
    }
  
  print(json.dumps(res))
  #print(json.dumps(res, indent=2))


if __name__ == "__main__":
  # execute only if run as a script
  main()
