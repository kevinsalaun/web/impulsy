
const gameRoomService = require('./gameRoom2.service');

function listeners(io, gameSocket) {
    console.log('gameroom2 loaded');
    //gameRoomService.flush();

    // Host Events
    gameSocket.on('hostCreateNewGame', hostCreateNewGame);
    gameSocket.on('hostAddMedia', hostAddMedia);
    gameSocket.on('hostDelMedia', hostDelMedia);
    gameSocket.on('hostKickPlayer', hostKickPlayer);
    gameSocket.on('hostBanPlayer', hostBanPlayer);
    gameSocket.on('hostPrepareGame', hostPrepareGame);
    gameSocket.on('hostNextTrack', hostNextTrack);
    /*gameSocket.on('hostCountdownFinished', hostStartGame);*/

    // Player Events
    gameSocket.on('playerJoinGame', playerJoinGame);
    /*gameSocket.on('playerMove', playerMove);*/

    // All Events
    gameSocket.on('gameSummary', gameSummary);
    gameSocket.on('attachScreen', attachScreen);
    gameSocket.on('detachScreen', detachScreen);
    gameSocket.on('leaveGame', leaveGame);
    gameSocket.on('publicRoomList', publicRoomList);
    gameSocket.on('disconnect', connectionLost);


    /* *******************************
     *       HOST FUNCTIONS          *
     ******************************* */
    function hostCreateNewGame(userFingerprint, settings, cb) {
        console.log('creation of a new game room');
        gameRoomService.hostCreateNewGame(userFingerprint, settings, gameSocket)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    };

    function hostAddMedia(userFingerprint, roomId, mediaData, cb) {
        console.log('host adds a media');
        gameRoomService.hostAddMedia(io, userFingerprint, roomId, mediaData)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    };

    function hostDelMedia(userFingerprint, roomId, mediaId, cb) {
        console.log('host deletes a media');
        gameRoomService.hostDelMedia(io, userFingerprint, roomId, mediaId)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    };

    function hostKickPlayer(userFingerprint, roomId, playerId, cb) {
        console.log('host kicks a player');
        gameRoomService.hostKickPlayer(io, userFingerprint, roomId, playerId)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    };

    function hostBanPlayer(userFingerprint, roomId, playerId, cb) {
        console.log('host bans a player');
        gameRoomService.hostBanPlayer(io, userFingerprint, roomId, playerId)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    }

    function hostPrepareGame(userFingerprint, roomId, cb) { //go to game Vue
        console.log('host prepares the game room');
        gameRoomService.hostPrepareGame(io, userFingerprint, roomId)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    }

    function hostNextTrack(userFingerprint, roomId, cb) { // get the next track and sync clients with a countdown
        console.log('host prepares the game room');
        gameRoomService.hostNextTrack(io, userFingerprint, roomId)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    }


    /* *****************************
     *     PLAYER FUNCTIONS        *
     ***************************** */
    function playerJoinGame(userFingerprint, roomId, isScreen, screenDescription, cb) {
        console.log('a player join the game');
        gameRoomService.playerJoinGame(io, userFingerprint, roomId, isScreen, screenDescription, gameSocket)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    };


     /* *****************************
      *             ALL             *
      ***************************** */
    function gameSummary(userFingerprint, roomId, cb) {
        console.log('get game summary');
        gameRoomService.gameSummary(io, userFingerprint, roomId, gameSocket)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    };

    function attachScreen(userFingerprint, roomId, playerId, screenId, cb) {
        console.log('attach a screen');
        gameRoomService.gameSummary(io, userFingerprint, roomId, playerId, screenId)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    };

    function detachScreen(userFingerprint, roomId, playerId, cb) {
        console.log('attach a screen');
        gameRoomService.gameSummary(io, userFingerprint, roomId, playerId)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    };

    function leaveGame(userFingerprint, roomId, cb) {
        console.log('a player leave the game');
        gameRoomService.leaveGame(io, userFingerprint, roomId, gameSocket)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    };

    function publicRoomList(userFingerprint, cb) {
        console.log('get public rooms');
        gameRoomService.publicRoomList(userFingerprint)
            .then((data) => {
                cb(data);
            })
            .catch((err) => {
                cb({
                    error: true,
                    errorMsg: err.toString()
                });
            })
    };

    function connectionLost() {
        console.log('lost of a player');
        for (let i=0; i<gameSocket.rooms.length; i++) {
            gameRoomService.leaveGameFromSocket(io, gameSocket.rooms[i], gameSocket);
        }
    }
}

module.exports = {
  listeners,
};