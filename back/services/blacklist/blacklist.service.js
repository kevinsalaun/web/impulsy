﻿﻿//const config = require('../../config');
//const errorMsg = require('../../messages/blacklist.errors');
const Blacklist = require('./blacklist.model');

async function ban(ip) {
  const record = new Blacklist({ ip: ip });
  const execState = await record.save();
  console.log("blacklist ban "+ip+": "+execState.createdDate);
}

async function unban(ip) {
  const execState = await Blacklist.deleteOne({ ip: ip });
  console.log("blacklist unban "+ip+": "+execState.deletedCount);
}

async function isBanned(ip) {
  const record = await Blacklist.findOne({ ip: ip });
  return (record != null);
}

async function removeAll() {
  const execState = await Blacklist.deleteMany({});  
  console.log("blacklist removeAll : "+execState.deletedCount+", found "+execState.n);
}

module.exports = {
  ban,
  unban,
  isBanned,
  removeAll
};
