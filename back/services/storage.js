class Storage {
  constructor() {
    const config = require('../config');
    this.mongoose = require('mongoose');
    this.mongoose.set('useUnifiedTopology', true);
    this.mongoose.connect(config.db.uri+'/'+config.db.database, { 
      useCreateIndex: true, 
      useNewUrlParser: true, 
      useFindAndModify: false
    });
    this.mongoose.Promise = global.Promise;
    this.Schema = this.mongoose.Schema;
  }

  attachVirtualFields(schema, vfArray) {
    for(var i=0; i<vfArray.length; i++) {
      const item = vfArray[i];
      schema.virtual(item.field).get(item.fct);
    }
  }

  createSchema (name, structure, vfArray=null) {
    const newSchema = new this.Schema(structure);
    newSchema.set('toJSON', { virtuals: true });
    if (vfArray) {
      attachVirtualFields(newSchema, vfArray);
    }
    return this.mongoose.model(name, newSchema);
  }

  model (name, structure) {
    try {
      return this.mongoose.model(name);
    } catch (MissingSchemaError) {
      const newSchema = new this.Schema(structure);
      newSchema.set('toJSON', { virtuals: true });
      return this.mongoose.model(name, newSchema);
    }
  }
}

class Singleton {
  constructor() {
    if (!Singleton.instance) {
      Singleton.instance = new Storage();
    }
  }

  getInstance() {
    return Singleton.instance;
  }
}

module.exports = Singleton;