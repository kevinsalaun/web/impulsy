function formatTime(seconds) {
    let h = Math.floor(seconds / 3600);
    let m = Math.floor((seconds % 3600) / 60);
    let s = Math.floor(seconds % 60);
    return [h, m > 9 ? m : h ? '0' + m : m || '0', s > 9 ? s : '0' + s]
        .filter(a => a)
        .join(':');
}

function buffer2src(buffer) {
    return file2url(new Blob([buffer]));
}

function file2url(resource) {
    return window.URL.createObjectURL(resource);
}

function editObservableJson(thisSet, list, index, data) {
    let content = Object.assign({}, list[index], data);
    thisSet(list, index, content);
}

function deleteObservableJson(thisDelete, list, index) {
    if (list && list.length != 0) {
        thisDelete(list, index);
    }
}

module.exports = {
    formatTime,
    buffer2src,
    file2url,
    editObservableJson,
    deleteObservableJson
};
