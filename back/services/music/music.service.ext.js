const ps = require('python-shell');
const fs = require('fs');
const ytdl = require('ytdl-core');
const randomToken = require('random-token');
const ffmpeg = require('fluent-ffmpeg');
const path = require('path');
const musicMetadata = require('music-metadata');

async function getFileID(musicLocation) {
  return new Promise(function(resolve, reject) {
    var fileID = randomToken(16);
    while (fs.existsSync(musicLocation+fileID)) {
      fileID = randomToken(16);
    }
    resolve(fileID);
  });
}

async function downloadYtb(filePath, ytTag) {
  return new Promise(function(resolve, reject) {
    console.log('download file...');

    // Upload the youtube file
    const extension = '.mp4';
    const url = 'https://www.youtube.com/watch?v='+ytTag
    //const videoID = ytdl.getURLVideoID(url);

    // Create a reference to the stream of the video being downloaded.
    let videoObject = ytdl(url, {
      filter: 'audioonly',
      quality: 'highestaudio'
    });

    videoObject
      .pipe(fs.createWriteStream(filePath))
      .on('error', (err) => {
        console.log('download error:', err.message);
        reject(err);
      })
      .on('finish', () => {
        console.log("download completed")
        resolve();
      });

    
    // https://www.npmjs.com/package/ytdl-core
  });
}

/*async function downloadLocal(filePath, req) {
  return new Promise(function(resolve, reject) {
    console.log('download file...');
    // TODO: Upload local file
    // ...
    var data = '';
    req.on('data', function (chunk) {
      data += chunk;
    });
    
    req.on('end', function () {
      console.log('File uploaded, data:', data);
      resolve();
    });
  });
}*/

async function convert2wav(filePath) {
  return new Promise(function(resolve, reject) {
    console.log('beginning of the conversion...');
    const filePathWithoutExt = filePath.replace(/\.[^/.]+$/, '');
    const newFile = filePathWithoutExt+'.wav';
    ffmpeg(filePath)
      .toFormat('wav')
      .on('error', (err) => {
        console.log('ffmpeg error:', err.message);
        reject(err);
      })
      .on('end', () => {
        console.log('ffmpeg processing finished !');
        resolve(newFile);
      })
      .save(newFile);
  });
}

async function audio2data(musicLocation, filename) {
  return new Promise(function(resolve, reject) {
    console.log('extract data...');
    console.log('filename:',filename);
    
    let options = {
      mode: 'json',
      pythonPath: 'libraries/python/env/bin/python', 
      pythonOptions: ['-u'], // get print results in real-time
      scriptPath: 'libraries/python/',
      args: [musicLocation, filename] // path then fileNames
    };
    
    ps.PythonShell.run('audio2data.py', options, function (err, results) {
      if (err) reject(err);
      resolve(results);
    });
  });
}

/*function cleanMusic(filePath, newFile) {
  fs.unlinkSync(filePath);
  fs.unlinkSync(newFile);
}*/

async function cleanMusicDir(musicLocation) {
  fs.readdir(musicLocation, (err, files) => {
    if (err) throw err;
    for (const file of files) {
      fs.unlink(path.join(musicLocation, file), err => {
        if (err) throw err;
      });
    }
  });  
}

function cleanFiles(files) {
  // Remove files
  for (var idx in files) {
    if (fs.existsSync(files[idx])) {
      fs.unlinkSync(files[idx]);
    }
  }
}

async function treatAndSaveMusic(filePath, description) {
  return new Promise(function(resolve, reject) {
    const extractData = function(musicLocation, newFile) {
      if (newFile && newFile != '') {
        // Check duration
        musicMetadata.parseFile(newFile, {native: true})
          .then(tag => {
            //console.log('tag:',tag);
            const duration = tag.format.duration;

            if(duration <= 720) { // 12 minutes = 720 seconds
              // Extract audio data
              const filename = path.basename(newFile);
              audio2data(musicLocation, filename)
                .then((data) => {
                  cleanFiles([filePath, newFile]);
                  // Save data
                  if (data) {
                    /*console.log('description:', description);
                    console.log('audio data:', data);
                    console.log('filename:', filename);
                    console.log('data[0][filename].channels:', data[0][filename].channels);*/
                    
                    
                    description.duration = duration;
                    const json = {
                      ...description,
                      spectrum: data[0][filename],
                      gamemode: 'classic'
                    };
                    //console.log('json before return:', json);
                    
                    resolve(json);
                  } else {
                    cleanFiles([filePath, newFile]);
                    reject('No data extracted');
                  }
                })
                .catch((err) => {
                  cleanFiles([filePath, newFile]);
                  reject('Error during the audio data extraction');
                })
            } else {
              cleanFiles([filePath, newFile]);
              reject('Max duration exceeded');
            }
          })
          .catch(err => {
            cleanFiles([filePath, newFile]);
            reject('music-metadata error');
          });
      } else {
        cleanFiles([filePath]);
        reject('Error with filepath');
      }
    }

    if(filePath && description) {
      const supported = [ '.mp3', '.wav' ];
      const musicLocation = path.dirname(filePath);
      var newFile = filePath;
      if (!supported.includes(path.extname(filePath))) {
        // Convert to wave format
        convert2wav(filePath).then((file) => {
          newFile = file;
          extractData(musicLocation, newFile);
        })
      } else {
        extractData(musicLocation, newFile);
      }
    } else {
      cleanFiles([filePath]);
      reject('Missing arguments');
    }
  });
}

module.exports = {
  getFileID,
  convert2wav,
  downloadYtb,
  //downloadLocal,
  audio2data,
  //cleanMusic,
  cleanMusicDir,
  treatAndSaveMusic
}

/*
 Example of request:
 PUT   /music
 body = {
	"title": "Ylvis - The Fox (What Does The Fox Say?)",
	"gamemode": "classic",
	"platform": "youtube",
	"ytTag": "jofNR_WkoCE"
}
*/