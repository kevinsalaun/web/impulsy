const path = require("path");
const fs = require("fs");

class WebSocket {
  constructor() {
    this.io = null;
  }

  init(app) {
    this.io = require('socket.io')(app);

    //let websocketManager = path.basename(__filename);
    //let connection = 
    this.io.on('connection', (socket) => {
      console.log('new connection !!');
      
      // auto load websocket files
      fs.readdirSync(__dirname).forEach((filename) => {
        let longExt = filename.substring(filename.indexOf('.'));
        //let shortExt = filename.substr(filename.lastIndexOf('.'));
        if (longExt == '.controller.js') {
          require("./" + filename).listeners(this.io, socket);
        }
      });
    });
  }
}

class Singleton {
  constructor() {
    if (!Singleton.instance) {
      Singleton.instance = new WebSocket();
    }
  }

  getInstance() {
    return Singleton.instance;
  }
}

module.exports = Singleton;