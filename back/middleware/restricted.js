const authErrorMsg = require('../messages/auth.errors');

module.exports = (req, res, next) => {
  //console.log('restricted->run');
  //console.log('restricted->userFingerprint',req.userFingerprint);
  
  if(!req.userFingerprint) {
    console.log('restricted->request blocked');
    next(authErrorMsg.missingToken);
    return;
  }
  next();
};