const config = require('../config');
//const errorMsg = require('../messages/statistics.errors');
const blacklistService = require('../services/blacklist/blacklist.service');
const activityService = require('../services/activity/activity.service');

module.exports = (req, res, next) => {
  //console.log('statistics->run');
  
  // checks if client ip is blacklisted
  const ip = req.connection.remoteAddress;
  activityService.getActivity(ip)
    .then((data) => {
      if(data != null) {
        //console.log('activity:', data);
      }
    });

  if(ip) {
    blacklistService.isBanned(ip)
      .then(banned => {
        if(!banned) {
          //console.log('statistics - ip NOT blacklisted');
          // adds/updates the statistics
          activityService.record(ip)
            .then(() => {
              // Hand over
              next();
            });
        } else {
          //console.log('statistics - ip blacklisted');
          next();
        }
      });
  } else {
    //console.log('statistics - no ip');
    next();
  }  
};