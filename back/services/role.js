const roleEnum = {
    "player": 1, 
    "moderator": 2, 
    "administrator": 3
}
Object.freeze(roleEnum)

module.exports = roleEnum;
