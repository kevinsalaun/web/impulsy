var axios = require('axios');
var config = require('../config');

function getErrorMsgByID(errorList, id) {
    var res = null;
    for (let idx in errorList) {
        const e = errorList[idx];
        if (e.id == id) {
            res = e.msg;
            break;
        }
    }
    return res;
}

function getErrorMsgByIdx(errorList, idx) {
    return errorList[idx].msg;
}

async function registration(errorList, user) {
    return new Promise(function(resolve, reject) {
        var errors = [];

        console.log('onSubmitRegistration:');

        errors = errors.concat(compliantUserForm(errorList, user));

        if (errors.length == 0) {
            console.log('send axios request');
            axios
                .put(config.backApi.uri + config.backApi.routes.user.create, {
                    username: user.username,
                    email: user.email,
                    birthday: user.birthday,
                    password: user.password
                })
                .then(resp => {
                    console.log('connected !!');
                    console.log('data:', resp.data);
                    resolve({
                        order: 'login',
                        resp: resp.data
                    });
                })
                .catch(err => {
                    console.log('data.error:', err.response.data.error);
                    errors.push(
                        getErrorMsgByIdx(errorList, err.response.data.error)
                    );
                    reject(errors);
                });
        } else {
            reject(errors);
        }
    });
}

async function login(errorList, user) {
    return new Promise(function(resolve, reject) {
        var errors = [];

        console.log('onSubmitLogin:');
        // conditions
        console.log('user:', user);

        if (user && (user.username || user.email)) {
            console.log('ho !!');
            if (user.password == '') {
                errors.push(getErrorMsgByID(errorList, 'incompleteForm'));
            }
        } else {
            errors.push(getErrorMsgByID(errorList, 'incompleteForm'));
        }

        if (errors.length == 0) {
            console.log('send axios request');
            var filter = {
                username: user.username,
                password: user.password
            };

            if (user.username == '') {
                filter = {
                    email: user.email,
                    password: user.password
                };
            }

            axios
                .post(
                    config.backApi.uri +
                        config.backApi.routes.user.authenticate,
                    filter
                )
                .then(resp => {
                    console.log('connected !!');
                    console.log('data:', resp.data);
                    resolve({
                        order: 'login',
                        resp: resp.data
                    });
                })
                .catch(err => {
                    console.log('data.error:', err.response.data.error);
                    errors.push(
                        getErrorMsgByIdx(errorList, err.response.data.error)
                    );
                    reject(errors);
                });
        } else {
            reject(errors);
        }
    });
}

async function update(errorList, id, token, user) {
    return new Promise(function(resolve, reject) {
        var errors = [];

        console.log('onSubmitUpdate:');

        errors = errors.concat(compliantUserForm(errorList, user, true));

        if (errors.length == 0) {
            console.log('send axios request: id =', id, '; token =', token);
            axios
                .post(
                    config.backApi.uri +
                        config.backApi.routes.user.update.replace('<id>', id),
                    {
                        ...user
                    },
                    {
                        headers: {
                            Authorization: 'Bearer ' + token,
                            'Content-Type': 'application/json'
                        },
                        timeout: 1000
                    }
                )
                .then(resp => {
                    console.log('connected !!');
                    console.log('data:', resp.data);
                    resolve({
                        order: 'login',
                        resp: resp.data
                    });
                })
                .catch(err => {
                    console.log('data.error:', err.response.data.error);
                    errors.push(
                        getErrorMsgByIdx(errorList, err.response.data.error)
                    );
                    reject(errors);
                });
        } else {
            reject(errors);
        }
    });
}

async function del(errorList, id) {
    return new Promise(function(resolve, reject) {
        var errors = [];

        console.log('onSubmitDelete:');
        console.log('send axios request');
        axios
            .delete(
                config.backApi.uri +
                    config.backApi.routes.user.delete.replace('<id>', id)
            )
            .then(resp => {
                console.log('removed !!');
                resolve();
            })
            .catch(err => {
                console.log('data.error:', err.response.data.error);
                errors.push(
                    getErrorMsgByIdx(errorList, err.response.data.error)
                );
                reject(errors);
            });
    });
}

function compliantUserForm(errorList, user, update = false) {
    //Password policy
    /*
    1 lowercase character                        (?=.*[a-z])
    1 uppercase character                        (?=.*[A-Z])
    1 numeric character                            (?=.*[0-9])
    1 special character                            (?=.[!@#\$%\^&\*])
    */

    var errors = [];

    if (user == null) {
        errors.push(getErrorMsgByID(errorList, 'incompleteForm'));
    } else {
        if (
            user.username == null ||
            user.email == null ||
            user.password == null ||
            user.birthday == null
        ) {
            if (!update) {
                errors.push(getErrorMsgByID(errorList, 'incompleteForm'));
            }
        }
    }

    if (errors.length == 0) {
        if (user.username) {
            if (
                user.username.length > 12 ||
                user.username.length < 4 ||
                !user.username.match(/^[A-Za-z0-9]+$/)
            ) {
                errors.push(
                    getErrorMsgByID(errorList, 'usernamePolicyWarning')
                );
            }
        } else {
            if (!update || (update && !user.email)) {
                errors.push(
                    getErrorMsgByID(errorList, 'usernamePolicyWarning')
                );
            }
        }

        if (user.email) {
            if (
                !user.email.match(
                    /^[\w.-]+@\w(?:[\w-]{0,61}\w)?(?:\.\w(?:[\w-]{0,61}\w)?)*$/
                )
            ) {
                errors.push(getErrorMsgByID(errorList, 'emailPolicyWarning'));
            }
        } else {
            if (!update || (update && !user.username)) {
                errors.push(getErrorMsgByID(errorList, 'emailPolicyWarning'));
            }
        }

        if (user.password) {
            if (
                user.password.length > 25 ||
                user.password.length < 8 ||
                !user.password.match(
                    /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*_-])/
                )
            ) {
                errors.push(
                    getErrorMsgByID(errorList, 'passwordPolicyWarning')
                );
            }
        } else {
            if (!update) {
                errors.push(
                    getErrorMsgByID(errorList, 'passwordPolicyWarning')
                );
            }
        }

        if (user.password && user.passwordConfirm) {
            if (user.password != user.passwordConfirm) {
                errors.push(getErrorMsgByID(errorList, 'badPasswordConfirm'));
            }
        } else {
            if (!update) {
                errors.push(getErrorMsgByID(errorList, 'badPasswordConfirm'));
            }
        }

        if (user.birthday) {
            if (!user.birthday instanceof Date) {
                errors.push(getErrorMsgByID(errorList, 'birthdayDateFormat'));
            }
        } else {
            if (!update) {
                errors.push(getErrorMsgByID(errorList, 'birthdayDateFormat'));
            }
        }

        if (!update) {
            if (user.agreement && user.agreement != 'accept') {
                errors.push(getErrorMsgByID(errorList, 'userAgreementRefused'));
            }
        }

        /*if(user.role) {
            errors.push(getErrorMsgByID(errorList, 'nonexistentRole'));
        } else {
            if(!roleEnum[user.role]) {
                errors.push(getErrorMsgByID(errorList, 'nonexistentRole'));
            }
        }*/
    }

    return errors;
}

module.exports = {
    registration,
    login,
    update,
    del
};
