// numbering: 1 - 40

module.exports = {
    userAlreadyExists: {
        webCode: 400,
        refErr: 1,
    },
    emailAlreadyUsed: {
        webCode: 400,
        refErr: 2,
    },
    badLogin: {
        webCode: 400,
        refErr: 3,
    },
    userNotFound: {
        webCode: 404,
        refErr: 4,
    },
    tokenGeneration: {
        webCode: 500,
        refErr: 5,
    },
    incompleteForm: {
        webCode: 400,
        refErr: 6,
    },
    usernamePolicyWarning: {
        webCode: 400,
        refErr: 7,
    },
    emailPolicyWarning: {
        webCode: 400,
        refErr: 8,
    },
    passwordPolicyWarning: {
        webCode: 400,
        refErr: 9,
    },
    missingUserLogin: {
        webCode: 400,
        refErr: 10,
    },
    birthdayDateFormat: {
        webCode: 400,
        refErr: 11,
    },
    nonexistentRole: {
        webCode: 400,
        refErr: 12,
    }
}