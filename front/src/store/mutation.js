export default {
    login(state, data) {
        state.user.auth.token = data.token;
        state.user.auth.expire = new Date(data.expire);
        state.user.auth.userFingerprint = data.userFingerprint;
        /////
        state.user.info = data.info;
        state.user.settings = data.settings;
    },
    
    logout(state) {
        state.user.auth.token = null;
        state.user.auth.expire = null;
        state.user.auth.userFingerprint = null;
        /////
        state.user.info = {};
        state.user.settings = {};
    },

    setTheme(state, theme) {
        state.user.settings.theme = theme;
    },

    setLanguage(state, language) {
        state.user.settings.language = language;
    }
}