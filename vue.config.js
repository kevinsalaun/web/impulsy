module.exports = {
  lintOnSave: false,
  configureWebpack: {
    devServer: {
      host: 'frontend-impulsy.local',
      port: '8080'
    }
  }
}
