const express = require('express');
const restricted = require('../../middleware/restricted');
const gameRoomService = require('./gameRoom.service');

const router = express.Router();

// Set routes
router.get('/', roomList);
router.put('/', restricted, booking);
router.delete('/', restricted, removeAll);
router.get('/:id', restricted, roomInfo);
router.delete('/:id', restricted, remove);
router.post('/:id/block', restricted, blockBooking);
router.post('/:id/join', restricted, playerJoin);
router.post('/:id/leave', restricted, playerLeave);
router.post('/:id/kick', restricted, kickPlayer);
router.post('/:id/lockPlaylist', restricted, lockPlaylist);
router.post('/:id/nextTrack', restricted, nextTrack);
module.exports = router;

// Set actions
/**
 * Get the list of game rooms
 * @route GET /
 * @group gameRoom - Operations about game room
 * @param {String} multiplayerMode.body - multiplayer mode
 * @param {String} gameMode.body - game mode
 * @param {String} difficulty.body - game difficulty
 */
function roomList(req, res, next) {
  gameRoomService.roomList(
    req.body.multiplayerMode, 
    req.body.gameMode, 
    req.body.difficulty
  )
    .then((data) => res.json(data))
    .catch(err => next(err));
}

/**
 * Get information about a game room
 * @route GET /:id
 * @group gameRoom - Operations about game room
 * @param {String} id.query.required - game room id
 * @param {String} userId.body - user to add in the player list
 */
function roomInfo(req, res, next) {
  console.log("gameRoom.controller->playerJoin");
  gameRoomService.roomInfo(
    req.params.id, 
    req.userFingerprint.id
  )
    .then((data) => res.json(data))
    .catch(err => next(err));
}

/**
 * Book a game room
 * @route PUT /
 * @group gameRoom - Operations about game room
 * @param {String} owner.body.required - owner's id
 * @returns {object} the room id.
 */
function booking(req, res, next) {
  console.log("gameRoom.controller->booking");
  gameRoomService.booking(
    req.userFingerprint.id,
    req.body.settings
  )
    .then(roomId => res.json({id: roomId}))
    .catch(err => next(err));
}

/**
 * Block the game room booking
 * @route POST /:id/block
 * @group gameRoom - Operations about game room
 * @param {String} id.query.required - game room id
 * @param {String} playlist.body - playlist to save
 */
function blockBooking(req, res, next) {
  console.log("gameRoom.controller->blockBooking");
  console.log('gameRoomService.blockBooking:', req.params);
  console.log('gameRoomService.blockBooking:', req.body.playlist);
  gameRoomService.blockBooking(
    req.params.id, 
    req.userFingerprint.id, 
    req.body.playlist
  )
    .then(() => res.json({}))
    .catch(err => next(err));
}

/**
 * A player joins the game room
 * @route POST /:id/join
 * @group gameRoom - Operations about game room
 * @param {String} id.query.required - game room id
 * @param {String} userId.body - user to add in the player list
 */
function playerJoin(req, res, next) {
  console.log("gameRoom.controller->playerJoin");
  gameRoomService.playerJoin(
    req.params.id, 
    req.userFingerprint.id
  )
    .then((data) => res.json(data))
    .catch(err => next(err));
}

/**
 * A player leaves the game room
 * @route POST /:id/leave
 * @group gameRoom - Operations about game room
 * @param {String} id.query.required - game room id
 * @param {String} userId.body - user to remove from the player list
 */
function playerLeave(req, res, next) {
  console.log("gameRoom.controller->playerLeave");
  gameRoomService.playerLeave(
    req.params.id, 
    req.userFingerprint.id
  )
    .then(() => res.json({}))
    .catch(err => next(err));
}

/**
 * Kick a player from the game room
 * @route POST /:id/kick
 * @group gameRoom - Operations about game room
 * @param {String} id.query.required - game room id
 * @param {String} userId.body - user to kick from the player list
 */
function kickPlayer(req, res, next) {
  console.log("gameRoom.controller->kickPlayer");
  gameRoomService.kickPlayer(
    req.params.id, 
    req.userFingerprint.id,
    req.body.userId
  )
    .then(() => res.json())
    .catch(err => next(err));
}

/**
 * Lock the game room playlist
 * @route POST /:id/lockPlaylist
 * @group gameRoom - Operations about game room
 * @param {String} id.query.required - game room id
 */
function lockPlaylist(req, res, next) {
  console.log("gameRoom.controller->lockPlaylist");
  gameRoomService.lockPlaylist(
    req.params.id,
    req.userFingerprint.id
  )
    .then(() => res.json({}))
    .catch(err => next(err));
}

/**
 * Get the next track of the playlist
 * @route POST :id/nextTrack
 * @group gameRoom - Operations about game room
 * @param {String} id.query.required - game room id
 */
function nextTrack(req, res, next) {
  console.log("gameRoom.controller->nextTrack");
  gameRoomService.nextTrack(
    req.userFingerprint.id,
    req.params.id
  )
    .then((data) => res.json(data))
    .catch(err => next(err));
}

/**
 * Remove the game room
 * @route DELETE /:id
 * @group gameRoom - Operations about game room
 * @param {String} id.query.required - game room id
 */
function remove(req, res, next) {
  console.log("gameRoom.controller->remove");
  gameRoomService.remove(req.params.id)
    .then(() => res.json({}))
    .catch(err => next(err));
}

/**
 * Remove all game rooms (for test only)
 * @route DELETE /
 * @group gameRoom - Operations about game room
 */
function removeAll(req, res, next) {
  gameRoomService.removeAll()
    .then(() => res.json({}))
    .catch(err => next(err));
}
